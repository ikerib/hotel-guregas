Registro de tareas de limpieza de GUREGAS
========================

Proyecto elaborado con Symfony2 para el control de tareas de limpieza

1) Notas de la instalación
--------------------------------

### Instalarar symfony2 normalmente y las siguientes librerias:
  * soap.so
  * libxml para php

### Solucionar tema de permisos

  * sudo chmod +a "_www allow delete,write,append,file_inherit,directory_inherit" app/cache app/logs
  * sudo chmod +a "gitek allow delete,write,append,file_inherit,directory_inherit" app/cache app/log
  * sudo chmod -R 777 web/uploads

### OTROS
  * En src/Gitek/RegistroBundle/Form/MasterType: Cambiar el id del tipousuario
  * En src/Gitek/RegistroBundle/Form/RegistroType: Cambiar el id del tipousuario
  * En src/Gitek/UsuarioBundle/Entity/UsuarioRepository: Cambiar el id del tipo por el de tipousuario
  * Tarea ojo con los ID en recepcion/asignar. Tienen que ser 1,2,3 en la base de datos
  * FOSUserBundle aldaketak:
    ** vendor/bundles/FOS/UserBundle/Controller/SecurityController.php
        *** loginAction funtzioan
            **** $csrfToken = $this->container->get('form.csrf_provider')->generateCsrfToken('authenticate');
            **** ondoren gehitu:
            **** $usuarios =  $this->container->get('fos_user.user_manager')->findUsers();
            **** y agregar la variable al response

### Para el error  is not a valid entity or mapped super class.
  * En UsuarioBundle\Entity\Usuario.php agregar @ORM\Entity
  * En UsuarioBundle\Entity\Tipousuario.php quitar @ORM\Table

### Crear los usuarios y roles para FOSUserBundle

  * admin     => ROLE_ADMIN
  * recepcion => ROLE_RECEPCION
  * encargado => ROLE_ENCARGADO
  * usuarios  => ROLE_LIMPIEZA
  * En password para los usuarios de limpieza es: limpieza

### Nomeclatura de los nombres de imagenes para el mapa
  * Habitacion - tarea - ocupado - realizacion - incidencia.png => EJEMPLO: 108-02-berdea-1-1.png

    ** Habitacion : 101, 102, 103 ...

    ** tarea:
      ** 01 : 1º Dia
      ** 02 : Repaso
      ** 03 : Completa

    ** Ocupado:
      ** gorria: si ocupado => SI OCUPADO EL NOMBRE SERÁ Habitacion-gorria.png => 101-gorria.png
      ** berdea: si libre

    ** Realización:
      ** 1: Realizado
      ** 2: En proceso
      ** 3: Sin comenzar

    ** Incidencia:
      ** 1: SI
      ** NO

### Probando REST
  * curl -i -H "Accept: application/json" -X POST -d "incidenciaid=9" http://hotel.local/app_dev.php/incidencias/22 > /Users/gitek/Desktop/rest.html

### ServicioWeb
  * http://www.hotelbeasain.com/webservice/wshotelbeasain.asmx?op=EstadoHabitacionesFechas

### deshabilitados
  * "sensio/buzz-bundle": "dev-master",
  * "knplabs/knp-gaufrette-bundle" : "dev-master",