<?php

namespace Gitek\RegistroBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpFoundation\Response;

use Gitek\RegistroBundle\Entity\Registro;
use Gitek\RegistroBundle\Entity\Registrodet;
use Gitek\RegistroBundle\Entity\Tarea;
use Gitek\RegistroBundle\Entity\Master;
use Gitek\RegistroBundle\Form\RegistroType;
use Gitek\RegistroBundle\Form\MasterType;

class UsuarioController extends Controller
{
    /**
     * Muestra el formulario de busqueda
     *
     */
    public function mapaAction()
    {
        $em = $this->getDoctrine()->getManager();

        $q = Date('Y-m-d');

        $user = $this->get('security.context')->getToken()->getUser();


        $registros = $em->getRepository('RegistroBundle:Registro')->registrosporfechausuario(Date($q), $user->getId());

        if (count($registros) == 0) {
            $master = new Master();
            $servicio = $this->get('gitek_mapatareas');
            $mapa = $servicio->getMapatareas($q);
            $usuario_por_defecto = $em->getRepository('UsuarioBundle:Usuario')->findOneById($this->container->getParameter('gitek.usuario_por_defecto'));

            for ($i = 1; $i <= 10; $i++) {
                $reg = new Registro();
                $reg->setMaster($master);
                $mitipotarea = $em->getRepository("HotelBundle:Tipotarea")->findOneById($mapa[$i]["Tarea"]);
                $reg->setTipotarea($mitipotarea);
                if (empty($q)) {
                    $reg->setFecha(new \DateTime());
                } else {
                    $niretime = strtotime($q);
                    $n = new \DateTime(date('Y-m-d', $niretime));
                    $reg->setFecha($n);
                }
                //habitacion
                if ($i < 10) {
                    $nombrehabitacion = "10" . $i;
                } else {
                    $nombrehabitacion = "110";
                }
                $hab = $em->getRepository('HotelBundle:Habitacion')->findOneByNombre($nombrehabitacion);
                $reg->setHabitacion($hab);

                // Usuario por defecto
                $reg->setUsuario($usuario_por_defecto);

                $em->persist($reg);
            }

            $em->flush();
            $registros = $em->getRepository('RegistroBundle:Registro')->registrosporfecha(Date($q));
            $mapa = $servicio->getMapatareas($q);
        }


        $registros = $em->getRepository('RegistroBundle:Registro')->registrosporfechausuario(Date($q), $user->getId());
        // Miramos si tiene detalles. Si no los tiene, marcamos como no comenzado.
        // El asunto es que si el usuario entra en la habitación poner
        // Comenzado = 1
        // pero si sale sin marcar nada, por ejemplo, solo mirar tareas, se queda en amarillo
        // que significa que esta trabajando en ello. No puede ser.

        foreach ($registros as $r) {
            if (!$r->getDetalles() == null) {
                if ($r->getDetalles()->count() == 0) {
                    $r->setComenzado(0);
                    $em->persist($r);
                }
            }
        }
        $em->flush();

        $usu = $user->getId();

        $servicio = $this->get('gitek_mapatareas');
        $mapa = $servicio->getMapatareas($q, $usu);

        return $this->render('RegistroBundle:Usuario:mapa.html.twig', array(
            'registros' => $registros,
            'q' => $q,
            'mapa' => $mapa,
        ));
    }

    /**
     *
     * Muestra el detalle de la habitación con las tareas a realizar.
     *
     */
    public function habitacionAction()
    {
        $em = $this->getDoctrine()->getManager();
        $request = $this->getRequest();
        $hab = $request->query->get('hab');
        $lan = $request->query->get('lan');
        $fec = $q = Date('Y-m-d');

        $registro = $em->getRepository('RegistroBundle:Registro')->tareasdelahabitacion($hab, $fec);
        // ladybug_dump( $registro->getTipotarea()->getTareas() );

        // foreach ($registro->getTipotarea()->getTareas() as $e) {
        // ladybug_dump( $e );
        // print_r($e->getCategorias()->getNombre());
        // ladybug_dump( $e->getCategorias() );
        // }
        // var_dump( $registro->getTipotarea()->getTareas() );
        $mistareas = $em->getRepository('RegistroBundle:Registro')->tareasdelregistroporfechausuario($hab, $fec);
        $incidencias = $em->getRepository('HotelBundle:Incidencia')->findAll();

        if (($registro->getCompletado() == 0) && ($registro->getComenzado() == 0)) {
            $registro->setCreatedat(new \DateTime()); // Hora de inicio de la tarea
        }

        $registro->setComenzado(1);
        $em->persist($registro);
        $em->flush();

        $session = $this->get('session');

        if ($session->has('filtro')) {
            if ($lan == "") {
                $session->set('filtro', 'Habitacion');
            } else {
                $session->set('filtro', $lan);
            }
        } else {
            $session->set('filtro', 'Habitacion');
        }

        return $this->render('RegistroBundle:Usuario:habitacion.html.twig', array(
            'registro' => $registro,
            'mistareas' => $mistareas,
            'incidencias' => $incidencias,
            'hab' => $hab,
        ));
    }

    public function portadaAction()
    {
        return $this->render('RegistroBundle:Usuario:portada.html.twig');
    }


    public function registrartareaAction()
    {

        if ($this->get('request')->isXmlHttpRequest()) {
            $em = $this->getDoctrine()->getManager();

            $registroid = $this->get('request')->request->get('registroid');
            $tareaid = $this->get('request')->request->get('tareaid');
            $registro = $em->getRepository('RegistroBundle:Registro')->findById($registroid);
            $registrodet = $em->getRepository('RegistroBundle:Registrodet')->findByRegistro($registroid);
            if (!$registrodet) {
                $registrodet = New Registrodet();
            }

            $tarea = $em->getRepository('HotelBundle:Tarea')->findById($tareaid);
            $registro->setUpdatedat(new \DateTime());
            $registro->setComenzado(1);
            $registro->setCompletado(0);

            $registrodet->setCreatedat($registro->getCreatedat);
            $registrodet->setUpdatedat(new \DateTime());
            $registrodet->setTarea($tarea);
            if ($registrodet->getCompletado() == 0) {
                $registrodet->setCompletado(1);
            } else {
                $registrodet->setCompletado(0);
            }
            $em->persist($registrodet);
            $em->persist($registro);
            $em->flush();

            return new Response("OK");

        } else {
            return new Response("KO0");
        }

    }

    public function registrarhabitacionAction()
    {

        if ($this->get('request')->isXmlHttpRequest()) {
            $em = $this->getDoctrine()->getManager();

            $registroid = $this->get('request')->request->get('registroid');

            $registro = $em->getRepository('RegistroBundle:Registro')->find($registroid);
            $registro->setComenzado(1);
            $registro->setCompletado(1);
            $registro->setUpdatedat(new \DateTime());
            $em->persist($registro);
            $em->flush();

            return new Response("OK");
        }
    }
}
