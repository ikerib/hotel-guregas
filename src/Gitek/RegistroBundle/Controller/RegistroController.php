<?php

namespace Gitek\RegistroBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use FOS\RestBundle\View\View;
use Gitek\RegistroBundle\Entity\Registro;
use Gitek\RegistroBundle\Entity\Registrodet;

class RegistroController extends Controller
{

    public function postRegistroAction($registroid)
    {
        $request = $this->get('request');
        $tareaid=$request->request->get('tareaid');

        $em         = $this->get('doctrine')->getManager();
        $registro   = $em->getRepository('RegistroBundle:Registro')->find($registroid);
        $tarea      = $em->getRepository('HotelBundle:Tarea')->find($tareaid);
        $tareas     = $registro->getDetalles();

        // Si no tiene detalles creamos
        if (count($tareas)==0) {

            $detalle = New Registrodet();
            $detalle->setRegistro($registro);
            $detalle->setTarea($tarea);
            $em->persist($detalle);
            $registro->addRegistrodet($detalle);
            $em->persist($registro);

        } else {
            $midetalle  = $em->getRepository('RegistroBundle:Registrodet')->getDetalleRegistroTarea($registroid,$tareaid);
            $ezabatua = 0;

            if ( count($midetalle)>0) {

                foreach ($registro->getDetalles()  as $d) {
                    if ( $d->getId() == $midetalle[0]->getId() ) {
                        $em->remove($d);
                        $em->persist($registro);
                        $ezabatua = 1;
                    }
                }
            }
            if ( $ezabatua == 0) {
                $detalle = New Registrodet();
                $detalle->setCreatedat($registro->getCreatedat());
                $detalle->setRegistro($registro);
                $detalle->setTarea($tarea);
                $em->persist($detalle);
                $registro->addRegistrodet($detalle);
                $em->persist($registro);
            }
        }

        $em->flush();

        return new Response("[{'erantzuna':'1'}]");
    } // "post_registro"     [POST] /registro/{$registroid}


    public function postRegistrocompletadoAction($registroid)
    {
        $request = $this->get('request');
        $completado=$request->request->get('completado');

        $em         = $this->get('doctrine')->getManager();
        $registro   = $em->getRepository('RegistroBundle:Registro')->find($registroid);

        $registro->setCompletado(floor($completado));
        $registro->setUpdatedat(new \DateTime());
        $em->persist($registro);

        $em->flush();

        return new Response("[{'erantzuna':'1'}]");
    } // "post_registrocompletado"     [POST] /registrocompletado/{$registroid}

}