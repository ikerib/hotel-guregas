<?php

namespace Gitek\RegistroBundle\Controller;

use Gitek\RegistroBundle\Entity\Master;
use Gitek\RegistroBundle\Entity\Registro;
use Gitek\RegistroBundle\Form\MasterType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;

class RecepcionController extends Controller
{

    public function asignarAction()
    {
        $em = $this->getDoctrine()->getManager();
        $master = new Master();
        $request = $this->getRequest();
        $q = $request->query->get('q');
        $servicio = $this->get('gitek_mapatareas');
        $mapa = $servicio->getMapatareas();
        $usuarios = $em->getRepository('UsuarioBundle:Usuario')->bilatuLangileak();

        if ('GET' === $request->getMethod()) {
            if (empty($q)) {
                $q = Date('Y-m-d');
            }
            $registros = $em->getRepository('RegistroBundle:Registro')->registrosporfecha(Date($q));
            if (!empty($registros)) {
                $r = $registros[0];
                if (!is_null($r->getMaster())) {
                    $master = $em->getRepository('RegistroBundle:Master')->find($r->getMaster()->getId());
                }
            } else {
                $master = new Master();
                $usuario_por_defecto = $em->getRepository('UsuarioBundle:Usuario')->findOneById($this->container->getParameter('gitek.usuario_por_defecto'));
                for ($i = 1; $i <= 10; $i++) {
                    $reg = new Registro();
                    $reg->setMaster($master);
                    $mitipotarea = $em->getRepository("HotelBundle:Tipotarea")->findOneById($mapa[$i]["Tarea"]);
                    $reg->setTipotarea($mitipotarea);
                    if (empty($q)) {
                        $reg->setFecha(new \DateTime());
                    } else {
                        $niretime = strtotime($q);
                        $n = new \DateTime(date('Y-m-d', $niretime));
                        $reg->setFecha($n);
                    }
                    //habitacion
                    if ($i < 10) {
                        $nombrehabitacion = "10" . $i;
                    } else {
                        $nombrehabitacion = "110";
                    }
                    $hab = $em->getRepository('HotelBundle:Habitacion')->findOneByNombre($nombrehabitacion);
                    $reg->setHabitacion($hab);

                    // Usuario por defecto
                    $reg->setUsuario($usuario_por_defecto);

                    $em->persist($reg);
                }
                $em->flush();
                $registros = $em->getRepository('RegistroBundle:Registro')->registrosporfecha(Date($q));
                $mapa = $servicio->getMapatareas($q);
            }

        } else {
            $registros = $em->getRepository('RegistroBundle:Registro')->registrosporfecha(Date('Y-m-d'));
            if (!empty($registros)) {
                $r = $registros[0];
                $master = $em->getRepository('RegistroBundle:Master')->find($r->getMaster()->getId());
            }
        }
        $form = $this->createForm(new MasterType(), $master);
        if ('POST' === $request->getMethod()) {
            $form->bind($request);
            if ($form->isValid()) {
                $em = $this->getDoctrine()->getManager();
                $em->persist($master);
                $em->flush();
                return $this->redirect($this->generateUrl('recepcion_asignar'));
            } else {
                print_r($form->getErrors());
            }
        } else {

        }
        return $this->render('RegistroBundle:Recepcion:asignar.html.twig', array(
            'master' => $master,
            'registros' => $registros,
            'usuarios' => $usuarios,
            'q' => $q,
            'mapa' => $mapa,
            'form' => $form->createView()
        ));
    }

    public function cambiartareaAction()
    {
        if ($this->get('request')->isXmlHttpRequest()) {
            $em = $this->getDoctrine()->getManager();
            $request = $this->getRequest();
            $hab = $request->query->get('hab');
            $tar = $request->query->get('tar');
            $tarnew = $request->query->get('tarnew');
            $fec = $request->query->get('q');
            $registro = $em->getRepository('RegistroBundle:Registro')->tareasdelahabitacion($hab, $fec);
            if ($registro) {
                $mitipotarea = $em->getRepository('HotelBundle:Tipotarea')->findOneById($tarnew);
                $registro->setTipotarea($mitipotarea);
            } else {
                $registro = New Registro();
                $registro->setFecha(new \DateTime());
            }
            $em->persist($registro);
            $em->flush();
            return new Response("OK");
        }
    }

    // "cambiartarea"     [POST] /cambiartarea

    public function estadohabitacionAction()
    {

        if ($this->get('request')->isXmlHttpRequest()) {

            $em = $this->getDoctrine()->getManager();
            $hab = $this->get('request')->request->get('hab');
            $balioa = $this->get('request')->request->get('balioa');
            $habitacion = $em->getRepository('HotelBundle:Habitacion')->findOneBy(array('nombre' => $hab));
            $habitacion->setOcupado($balioa);
            $habitacion->setUpdatedat(new \DateTime());
            $em->persist($habitacion);
            $em->flush();

            return new Response("OK");

        }
    }
}
