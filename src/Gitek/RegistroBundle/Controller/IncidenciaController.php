<?php

namespace Gitek\RegistroBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use FOS\RestBundle\View\View;
use Gitek\RegistroBundle\Entity\Registro;
use Gitek\HotelBundle\Entity\Incidencia;

class IncidenciaController extends Controller
{

    public function postIncidenciaAction($registroid)
    {
        $request = $this->get('request');
        $incidenciaid=$request->request->get('incidenciaid');
        // print_r("incidenciadid: " . $incidenciaid);
        $em          = $this->get('doctrine')->getManager();
        $registro    = $em->getRepository('RegistroBundle:Registro')->find($registroid);
        $incidencia  = $em->getRepository('HotelBundle:Incidencia')->find($incidenciaid);
        $incidencias = $registro->getIncidencias();
        // print_r("Lehena: " . $incidencia->getNombre());
        // Si no tiene incidencias creamos
        if (count($incidencias)==0) {
            // print_r("HEMEN");
            // print_r("Incidencia id : " . $incidenciaid);
            $registro->addIncidencia($incidencia);
            $em->persist($registro);
        } else {
            // print_r("HAN");
            // vale, si entramos aqui, significa que tenemos incidencias, eliminamos o creamos
            // segun el caso.
            //
            // Miramo si existe la incidencia
            $ezabatua = 0;
            // print_r("Tiene: " . count($incidencias));
            foreach ($incidencias  as $i) {
                // print_r($i->getNombre());
                // print_r("***");
                $miid = $i->getId();

                // print_r("Miid : " . $miid . "da.");
                // print_r("++++");

                if ( $miid == $incidencia->getId() ) {
                    $registro->getIncidencias()->removeElement($incidencia);
                    $em->persist($registro);
                    // print_r("jeje");
                    $ezabatua=1;
                    break;
                }
            }

            if ( $ezabatua == 0) {
                $registro->addIncidencia($incidencia);
                $em->persist($registro);
                // print_r("jujuju");
            }
        }

        $em->flush();


        // $response =  New response("ok",200);
        return new Response("[{'erantzuna':'1'}]");

// create a JSON-response with a 200 status code
// $response = new Response(json_encode(array('name' => $registro)));
// $response->headers->set('Content-Type', 'application/json');
// return $response;
        // $view = View::create();
        // return new Response($registro, 200, $view->getHeaders());

        // $view = View::create();
        // $view->setData($registro);
        // return new Response("OK");
        // return $view;
    } // "post_incidencia"     [POST] /incidencia/{$registroid}

}