    // Get the div that holds the collection of tags
    var collectionHolder = $('ul.registros');

    // setup an "add a tag" link
    var $addTagLink = $('<a href="#" class="add_tag_link">Detailea gehitu</a>');
    var $newLinkLi = $('<li></li>').append($addTagLink);

    jQuery(document).ready(function() {
        cargadatos();
        // add a delete link to all of the existing tag form li elements
        collectionHolder.find('li').each(function() {
            addTagFormDeleteLink($(this));
        });
        // add the "add a tag" anchor and li to the tags ul
        collectionHolder.append($newLinkLi);

        $addTagLink.on('click', function(e) {
            // prevent the link from creating a "#" on the URL
            e.preventDefault();

            // add a new tag form (see next code block)
            addTagForm();
        });
    });
    function addTagForm() {
        // Get the data-prototype we explained earlier
        var prototype = collectionHolder.attr('data-prototype');

        // Replace '$$name$$' in the prototype's HTML to
        // instead be a number based on the current collection's length.
        var newForm = prototype.replace(/\$\$name\$\$/g, collectionHolder.children().length-1);

        // Display the form in the page in an li, before the "Add a tag" link li
        var $newFormLi = $('<li></li>').append(newForm);
        $newLinkLi.before($newFormLi);

        // add a delete link to the new form
        addTagFormDeleteLink($newFormLi);
    }

    function addTagFormDeleteLink($tagFormLi) {
        var $removeFormA = $('<a href="#">delete this tag</a>');
        $tagFormLi.append($removeFormA);

        $removeFormA.on('click', function(e) {
            // prevent the link from creating a "#" on the URL
            e.preventDefault();

            // remove the li for the tag form
            $tagFormLi.remove();
        });
    }

    //configuramos el calendario
    $.datepicker.regional['es'] = {
        closeText: 'Cerrar',
        prevText: '&#x3c;Ant',
        nextText: 'Sig&#x3e;',
        currentText: 'Hoy',
        monthNames: ['Enero','Febrero','Marzo','Abril','Mayo','Junio',
    '               Julio','Agosto','Septiembre','Octubre','Noviembre','Diciembre'],
        monthNamesShort: ['Ene','Feb','Mar','Abr','May','Jun',
                            'Jul','Ago','Sep','Oct','Nov','Dic'],
        dayNames: ['Domingo','Lunes','Martes','Mi&eacute;rcoles','Jueves','Viernes','S&aacute;bado'],
        dayNamesShort: ['Dom','Lun','Mar','Mi&eacute;','Juv','Vie','S&aacute;b'],
        dayNamesMin: ['Do','Lu','Ma','Mi','Ju','Vi','S&aacute;'],
        weekHeader: 'Sm',
        dateFormat: 'yy-mm-dd',
        firstDay: 1,
        isRTL: false,
        showMonthAfterYear: false,
        yearSuffix: ''};

    $.datepicker.setDefaults($.datepicker.regional['es']);

    var now = new Date();
    var y = now.getFullYear();
    var m = parseInt(now.getMonth())+1;
    var d = now.getDate();

    $('#fetxa').val( y +"-" + m + "-" + d);

    $("#datepicker").datepicker({
        dateFormat: 'yy-mm-dd',
        onSelect: function(dateText, inst) {
            $("form input.somedatefield").val(dateText);
            $("#q").val(dateText);
            $('#bilatu').submit();
        }
    });

    if( $('#fetxa').val() ) {

        queryDate = $('#fetxa').val();

        var parsedDate = $.datepicker.parseDate('yy-mm-dd', queryDate);

        $('#datepicker').datepicker('setDate', parsedDate);
    }

    $('#master_usuario option').filter(function() {
            //may want to use $.trim in here
            return $(this).val() == $.trim($('#usu').val());
        }).attr('selected', true);

    function merge_arrays(arr1,arr2) {
        var mivar   = new Object();
        var cont    = arr1.length;

        for (i = 0; i < cont; i++) {
            var q = arr1[i];
            mivar[q] = arr2[i];
        }
        return mivar;
    }



    //
    // END
    // FORMULARIOAN DATUAK KARGATZEN
    //



    //
    // MAPAN KLIK EGITERAKOAN
    //
    var aHabitaciones="";
    var aTipotareas ="";
    var aMisindex="";
    var MisOcupados ="";
    function cargadatos() {

        //
        // FORMULARIOAN DATUAK KARGATZEN
        //
        var zenbat = 0;

        aHabitaciones = $('#mishabitaciones').val().split('#');
        aHabitaciones.pop(); // pop elimina el último elemento del array

        aTipotareas = $('#mistareas').val().split('#');
        aTipotareas.pop();

        aMisindex = $('#misindex').val().split('#');
        aMisindex.pop();

        MisOcupados = $('#misocupados').val().split('#');
        MisOcupados.pop();


        var aMisDatos = merge_arrays(aHabitaciones, aTipotareas);

        if (aMisDatos === "undefined") {
            aMisDatos = "";
        }
        // 105 => 2 'repaso
        // 106 => 3 'completo
        // ...
        var aMisDivs = merge_arrays(aHabitaciones, aMisindex);
        // 105 => 1 'hay que hacer -1'
        // 106 => 2 'hay que hacer -1'
        //...
        var aMisOcupados = merge_arrays(aHabitaciones, MisOcupados);
        // 105 => 1 //Ocupado
        // 106 => 0 //libre

        //cambiamos la imagen segun ocupado o no
        for (var hab in aMisOcupados) {
            console.log("sartu naiz1");
//            console.log("aMisOcupados: " + aMisOcupados.toSource());
            var midiv = $('a[rel="' + hab + '"]');
            var miimg = $(midiv).find('img');

            var izena;
            izena = $(miimg).attr('src').split('-');
            var gela = izena[0];
            var lana = izena[1];
            var kolorea = izena[2];

            if (aMisOcupados[hab] === 0) {
                kolorea = "berdea.png";
            } else {
                kolorea = "gorria.png";
            }

            var src = gela + '-' + lana + '-' + kolorea; // 1º Dia

//            $(miimg).attr('src', src);
        }

        // cambiamos la imagen segun tarea
        for (var hab in aMisDatos) {
            // console.log("sartu naiz2");
//            console.log("aMisDatos: "+ aMisDatos.toSource());
            var midiv = $('a[rel="' + hab + '"]');
            var miimg = $(midiv).find('img');
            // console.log("MIIMG:" +miimg);
            var izena;
            izena = $(miimg).attr('src').split('-');

            var mitarea=aMisDatos[hab];

            var gela = izena[0];
            var lana = izena[1];
            var kolorea = izena[2];

            var opc1 = gela + '-0' + mitarea + '-' + kolorea // 1º Dia
            var opc2 = gela + '-0' + mitarea + '-' + kolorea // Repaso
            var opc3 = gela + '-0' + mitarea + '-' + kolorea // completo

            var src = ($(miimg).attr('src') === opc1)
                ? opc2
                : opc3;

//            $(miimg).attr('src', src);

            zenbat = zenbat + 1;
        }


        if (!$('#fetxa').val()) { // si esta vacio
            var fec = $('#fetxa').val();
        } else {
            var fec = $("#datepicker").val();
        }

        var usu = $('#master_usuario').val();


        if (("" === usu) || (usu === "undefined")) {
            // alert("Seleccione operario primero");
            return false
        }
    }

    function mifunc(mirel, zenbatgarrena, misrc, mitarea) {
        //
        // FORMULARIOAN DATUAK KARGATZEN
        //

        var aHabitaciones = $('#mishabitaciones').val().split('#');
        aHabitaciones.pop(); // pop elimina el último elemento del array

        var aTipotareas = $('#mistareas').val().split('#');
        aTipotareas.pop();

        var aMisindex = $('#misindex').val().split('#');
        aMisindex.pop();

        var MisOcupados = $('#misocupados').val().split('#');
        MisOcupados.pop();


        var aMisDatos = merge_arrays(aHabitaciones, aTipotareas);
        var aMisDivs = merge_arrays(aHabitaciones, aMisindex);
        var aMisOcupados = merge_arrays(aHabitaciones, MisOcupados);

        if (!$('#fetxa').val()) { // si esta vacio
            var fec = $('#fetxa').val();
        } else {
            var fec = $("#datepicker").val();
        }

        var usu = $('#master_usuario').val();

        if (("" === usu) || (usu === "undefined")) {
            alert("Seleccione operario primero");
            return false;
        }

        var midiv = $('a[rel="' + mirel + '"]');
        var miimg = $(midiv).find('img');
        var izena;
        izena = $(miimg).attr('src').split('-');
        var gela = izena[0];
        var lana = izena[1];
        var kolorea = izena[2];

        // 1º Miramos si ya a sido agregado al detalle
        if (mirel in aMisDatos) {
            // bado

            // actualizamos la tarea
            aMisDatos[mirel]=mitarea;

            var tar1 = aMisDatos[mirel];
            var tar2 = parseInt(tar1) + 1;
            if (tar2 > 3) {
                tar2 = 1;
            }
            var tar3 = parseInt(tar2) + 1;
            if (tar3 > 3) {
                tar3 = 1;
            }

            zenbatgarrena = parseInt(aMisDivs[mirel])-1;
            var cmb_destino = "#master_registros_" + zenbatgarrena + "_tipotarea";
            $(cmb_destino).val(parseInt(aMisDatos[mirel]));

        } else {
            //ezto
            $addTagLink.click();
            //Bat por defekto jartzearren
            aMisDatos[mirel] = mitarea;

            //irudiaren izena osatu eta irudia kargatu
            $(miimg).attr('src', misrc);

            //datuak kopiatu formulariora klik egindako logelaren arabera
            var iusu = "master_registros_" + zenbatgarrena + "_usuario";
            $("#" + iusu).val(usu);

            var ifec = "master_registros_" + zenbatgarrena + "_fecha";
            $("#" + ifec).val(fec);

            var ihab = "master_registros_" + zenbatgarrena + "_habitacion";
            $("#" + ihab + " option").filter(
                function () {
                    return $(this).text() === mirel;
                }).attr('selected', true);

            var itip = "master_registros_" + zenbatgarrena + "_tipotarea";
            $("#" + itip + " option").filter(
                function () {
                    return $(this).val() === parseInt(aMisDatos[mirel]);
                }).attr('selected', true);

            // Guardamos los datos:
            var vHab = $('#mishabitaciones').val();
            if (vHab==="") {
                $('#mishabitaciones').val( mirel  + '#');
            } else {
                $('#mishabitaciones').val(vHab + mirel + '#' );
            }


            var aTar = $('#mistareas').val();
            if ( aTar === "" ) {
                $('#mistareas').val( parseInt(mitarea)  + '#');
            }   else {
                $('#mistareas').val(aTar + parseInt(mitarea) + '#');
            }


            var aInd = $('#misindex').val();
            var joe = parseInt(zenbatgarrena)+1;
            if ( aInd==="" ) {
                $('#misindex').val( joe + '#');
            } else {
                $('#misindex').val(aInd + joe + '#');
            }

            var aOcu = $('#misocupados').val();
            if ( aOcu==="" ) {
                $('#misocupados').val("0" + '#');
            } else {
                $('#misocupados').val(aOcu + "0" + '#');
            }
        }
    }
    //
    // END
    // MAPAN KLIK EGITERAKOAN
    //



    $('#btn_tarea').click(function(){
        $(this).addClass('btn-success');
        $('#btn_estado').removeClass('btn-success');
    });
    $('#btn_estado').click(function(){
        $(this).addClass('btn-success');
        $('#btn_tarea').removeClass('btn-success');
    });
    $('#botoia').click(function(){
        $('#form_datuak').submit();
    });


    $( init );

    function init() {

        $('#img_1dia').draggable({
            appendTo: "body",
            helper: "clone"
        });
        $('#img_repaso').draggable({
            appendTo: "body",
            helper: "clone"
        });
        $('#img_completo').draggable({
            appendTo: "body",
            helper: "clone"
        });
        $('#img_sale').draggable({
            appendTo: "body",
            helper: "clone"
        });
        $('#img_entra').draggable({
            appendTo: "body",
            helper: "clone"
        });

        $('#hotel_gela1').droppable({
            drop: handleDropEvent
        });
        $('#hotel_gela2').droppable({
            drop: handleDropEvent
        });
        $('#hotel_gela3').droppable({
            drop: handleDropEvent
        });
        $('#hotel_gela4').droppable({
            drop: handleDropEvent
        });
        $('#hotel_gela5').droppable({
            drop: handleDropEvent
        });
        $('#hotel_gela6').droppable({
            drop: handleDropEvent
        });
        $('#hotel_gela7').droppable({
            drop: handleDropEvent
        });
        $('#hotel_gela8').droppable({
            drop: handleDropEvent
        });
        $('#hotel_gela9').droppable({
            drop: handleDropEvent
        });
        $('#hotel_gela10').droppable({
            drop: handleDropEvent
        });

        function dimeNombreOcupado (ocupado) {
            if ( ocupado===0 ) {
                return "berdea";
            } else {
                return "gorria";
            }
        }

        function dimeNombreTarea ( e, tarea ) {
            switch (tarea)
            {
                case "img_1dia":
                    $(e).data("tarea","1dia");
                    return "01";
                    break;
                case "img_repaso":
                    $(e).data("tarea","repaso");
                    return "02";
                    break;
                case "img_completo":
                    $(e).data("tarea","completo");
                    return "03";
                    break;
                case "1dia":
                    $(e).data("tarea","1dia");
                    return "01";
                    break;
                case "repaso":
                    $(e).data("tarea","repaso");
                    return "02";
                    break;
                case "completo":
                    $(e).data("tarea","completo");
                    return "03";
                    break;
                default:
                    return dimeNombreTarea(e,$(e).data("tarea"));
            }
        }

        function dimeNombreImagen ( e, ocupado, tarea , habitacion) {

            return habitacion + "-" + dimeNombreTarea(e, tarea) + "-" + dimeNombreOcupado( ocupado) + ".png";

        }

        function handleDropEvent( event, ui, hab ) {
            var draggable = ui.draggable;
            var miid = draggable.attr('id');
            var miimg = $(this).children().children();
            var mihab = $(this).data("habitacion"); // nº habitacion
            var accion="";

            // Okupazioa bada aldatzen dena, data-ocupado aldatu behar da
            if ( miid === "img_entra"){
                $(this).data("ocupado",1);
                accion="entra";
                miid = $(this).data("tarea");
            } else if ( miid === "img_sale" ) {
                $(this).data("ocupado",0);
                accion="sale";
                miid = $(this).data("tarea");
            }

            var miocu = $(this).data("ocupado");

            // dependiendo si está ocupado o no

            var collectionHolder = $('ul.registros');
            var zenbatgarrena = collectionHolder.children().length-1;
            var mirel = $(this).data("habitacion");
            var mitarea = dimeNombreTarea(this, miid);

            switch (accion) {
                case "sale":
                    funcsalir(miimg, mirel, mitarea);
                    break;
                case "entra":
                    funcentrar(miimg, mirel, mitarea);
                    break;
                default:
//                    var misrc = "/bundles/registro/img/plano/" + dimeNombreImagen(this,miocu,miid,mihab);
                    var misrc = "/bundles/registro/img/plano2/" + dimeNombreImagen(this,miocu,miid,mihab);
                    $(miimg).attr("src", misrc);
                    mifunc(mirel,zenbatgarrena, misrc, mitarea);
                    break;
            }
        }
    }

    function funcsalir(dest, mirel, mitarea) {
        // console.log("Salir!");
        var aHabitaciones = $('#mishabitaciones').val().split('#');
        aHabitaciones.pop(); // pop elimina el último elemento del array

        var aTipotareas = $('#mistareas').val().split('#');
        aTipotareas.pop();

        var aMisindex = $('#misindex').val().split('#');
        aMisindex.pop();

        var MisOcupados = $('#misocupados').val().split('#');
        MisOcupados.pop();


        var aMisDatos = merge_arrays(aHabitaciones, aTipotareas);
        var aMisDivs = merge_arrays(aHabitaciones, aMisindex);
        var aMisOcupados = merge_arrays(aHabitaciones, MisOcupados);
        var tar='2';
        if (aMisDatos[mirel]) {
            tar=aMisDatos[mirel];
        }

//        TODO: Hau ere egin beharko da.

        // console.log(aMisDatos);
        // console.log(tar);
//        var misrc = "/bundles/registro/img/plano/" + mirel + "-0" + tar + "-berdea.png";
        var misrc = "/bundles/registro/img/plano2/" + mirel + "-0" + tar + "-berdea-0.png";
        $(dest).attr("src", misrc);

    }

    function funcentrar( dest, mirel, mitarea) {

        var aHabitaciones = $('#mishabitaciones').val().split('#');
        aHabitaciones.pop(); // pop elimina el último elemento del array

        var aTipotareas = $('#mistareas').val().split('#');
        aTipotareas.pop();

        var aMisindex = $('#misindex').val().split('#');
        aMisindex.pop();

        var MisOcupados = $('#misocupados').val().split('#');
        MisOcupados.pop();


        var aMisDatos = merge_arrays(aHabitaciones, aTipotareas);
        var aMisDivs = merge_arrays(aHabitaciones, aMisindex);
        var aMisOcupados = merge_arrays(aHabitaciones, MisOcupados);
        var tar="2";
        if (aMisDatos[mirel]) {
            tar=aMisDatos[mirel];
        }
        // TODO : Hau aldatu beharko zan, horrela tarea gabeko argazkia jartzeko.
        console.log(aMisDatos);
        console.log(tar);
//        var misrc = "/bundles/registro/img/plano/" + mirel + "-0" + tar + "-gorria.png";
        var misrc = "/bundles/registro/img/plano2/" + mirel + "-0" + tar + "-gorria.png";
        $(dest).attr("src", misrc);
    }