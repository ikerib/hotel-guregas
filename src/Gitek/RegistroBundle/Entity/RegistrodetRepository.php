<?php

namespace Gitek\RegistroBundle\Entity;

use Doctrine\ORM\EntityRepository;

class RegistrodetRepository extends EntityRepository
{

    public function getDetalleRegistroTarea($mireg,$mitar) {

        $em = $this->getEntityManager();

        $consulta = $em->createQuery('SELECT d, r, t FROM RegistroBundle:Registrodet d
                                    JOIN d.registro r
                                    JOIN d.tarea t
                                    WHERE r.id = :registroid
                                    AND t.id = :tareaid
                                    ORDER BY r.fecha desc
                                    ');
        $consulta->setParameter('registroid', $mireg);
        $consulta->setParameter('tareaid', $mitar);

        return $consulta->getResult();

//        return $consulta->getResult();
    }

}