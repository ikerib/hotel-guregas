<?php

namespace Gitek\RegistroBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Gitek\RegistroBundle\Entity\Registrodet
 *
 * @ORM\Table()
 * @ORM\Entity
 * @ORM\Entity(repositoryClass="Gitek\RegistroBundle\Entity\RegistrodetRepository"))
 */
class Registrodet
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;


   /**
     * @var datetime $created_at
     *
     * @ORM\Column(name="created_at", type="datetime")
     */
    private $created_at;

    /**
     * @var datetime $updated_at
     *
     * @ORM\Column(name="updated_at", type="datetime")
     */
    private $updated_at;

    /** @ORM\ManyToOne(targetEntity="Gitek\HotelBundle\Entity\Tarea", inversedBy="detalles") */
    protected $tarea;

    /** @ORM\ManyToOne(targetEntity="Gitek\RegistroBundle\Entity\Registro",inversedBy="detalles") */
    protected $registro;

    public function __construct()
    {
        $this->created_at = new \DateTime();
        $this->updated_at = new \DateTime();
    }

    // public function __toString()
    // {
    //     return $this->getNombre();
    // }


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set created_at
     *
     * @param datetime $createdAt
     */
    public function setCreatedAt($createdAt)
    {
        $this->created_at = $createdAt;
    }

    /**
     * Get created_at
     *
     * @return datetime
     */
    public function getCreatedAt()
    {
        return $this->created_at;
    }

    /**
     * Set updated_at
     *
     * @param datetime $updatedAt
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updated_at = $updatedAt;
    }

    /**
     * Get updated_at
     *
     * @return datetime
     */
    public function getUpdatedAt()
    {
        return $this->updated_at;
    }

    /**
     * Set registro
     *
     * @param Gitek\RegistroBundle\Entity\Registro $registro
     */
    public function setRegistro(\Gitek\RegistroBundle\Entity\Registro $registro)
    {
        $this->registro = $registro;
    }

    /**
     * Get registro
     *
     * @return Gitek\RegistroBundle\Entity\Registro
     */
    public function getRegistro()
    {
        return $this->registro;
    }

    /**
     * Set tarea
     *
     * @param Gitek\HotelBundle\Entity\Tarea $tarea
     */
    public function setTarea(\Gitek\HotelBundle\Entity\Tarea $tarea)
    {
        $this->tarea = $tarea;
    }

    /**
     * Get tarea
     *
     * @return Gitek\HotelBundle\Entity\Tarea
     */
    public function getTarea()
    {
        return $this->tarea;
    }
}