<?php

namespace Gitek\RegistroBundle\Entity;

use Doctrine\ORM\EntityRepository;
use Gitek\HotelBundle\Entity\Tarea;

class RegistroRepository extends EntityRepository
{

    public function registrosporfecha($mifecha) {

        $em = $this->getEntityManager();

        $consulta = $em->createQuery('SELECT r FROM RegistroBundle:Registro r
                                    WHERE r.fecha = :fecha
                                    ');
        $consulta->setParameter('fecha', $mifecha);

        return $consulta->getResult();
    }

    public function registrosporfechaordenadoporhabitacion($mifecha) {

        $em = $this->getEntityManager();

        $consulta = $em->createQuery('SELECT r FROM RegistroBundle:Registro r
                                    JOIN r.habitacion h
                                    WHERE r.fecha = :fecha
                                    ORDER BY h.nombre
                                    ');
        $consulta->setParameter('fecha', $mifecha);

        return $consulta->getResult();
    }

    public function registrosporfechaordenadoporhabitacionusuario($mifecha, $usu) {

        $em = $this->getEntityManager();

        $consulta = $em->createQuery('SELECT r FROM RegistroBundle:Registro r
                                    JOIN r.habitacion h
                                    JOIN r.usuario u
                                    WHERE r.fecha = :fecha
                                    AND u.id = :usu
                                    ORDER BY h.nombre
                                    ');
        $consulta->setParameter('fecha', $mifecha);
        $consulta->setParameter('usu', $usu);

        return $consulta->getResult();
    }

    public function registrosporfechausuario($mifecha,$usuid) {

        $em = $this->getEntityManager();

        $consulta = $em->createQuery('SELECT r FROM RegistroBundle:Registro r
                                    JOIN r.usuario u
                                    WHERE r.fecha = :fecha
                                    AND u.id = :id
                                    ');
        $consulta->setParameter('fecha', $mifecha);
        $consulta->setParameter('id', $usuid);

        return $consulta->getResult();
    }

    public function tareasdelahabitacion($hab,$fec) {

        $em = $this->getEntityManager();

        $consulta = $em->createQuery('SELECT r FROM RegistroBundle:Registro r
                                    JOIN r.tipotarea t
                                    JOIN r.habitacion h
                                    WHERE h.nombre = :hab
                                    AND r.fecha = :fec
                                    ');

        $consulta->setParameter('hab', $hab);
        $consulta->setParameter('fec', $fec);

        return $consulta->getOneOrNullResult();

    }

    public function tareasdelahabitacionportipo($regid) {

        $em = $this->getEntityManager();

        $consulta = $em->createQuery('SELECT t FROM HotelBundle:Tipotarea t
                                    WHERE t.id = :regid
                                    ');

        $consulta->setParameter('regid', $regid);

        return $consulta->getOneOrNullResult();

    }

    public function tareasdelahabitacionhab($hab,$fec) {

        $em = $this->getEntityManager();

        $consulta = $em->createQuery('SELECT r FROM RegistroBundle:Registro r
                                    JOIN r.tipotarea t
                                    JOIN r.habitacion h
                                    JOIN t.tareas tt
                                    JOIN tt.categorias c
                                    WHERE h.nombre = :hab
                                    AND r.fecha = :fec
                                    AND c.id = :cat
                                    ');

        $consulta->setParameter('hab', $hab);
        $consulta->setParameter('fec', $fec);
        $consulta->setParameter('cat', 2);

        return $consulta->getOneOrNullResult();

    }

    public function tareasdelahabitacionban($hab,$fec) {

        $em = $this->getEntityManager();

        $consulta = $em->createQuery('SELECT r FROM RegistroBundle:Registro r
                                    JOIN r.tipotarea t
                                    JOIN r.habitacion h
                                    WHERE h.nombre = :hab
                                    AND r.fecha = :fec');

        $consulta->setParameter('hab', $hab);
        $consulta->setParameter('fec', $fec);

        return $consulta->getOneOrNullResult();

    }

    public function tareasdelregistroporfechausuario ($hab,$fec) {
        $em = $this->getEntityManager();

        $consulta = $em->createQuery('SELECT t FROM HotelBundle:Tarea t
                                    JOIN t.detalles d
                                    JOIN d.registro r
                                    JOIN r.habitacion h
                                    WHERE h.nombre = :hab
                                    AND r.fecha = :fec
                                    ORDER BY t.updated_at
                                    ');
        $consulta->setParameter('hab', $hab);
        $consulta->setParameter('fec', $fec);

        return $consulta->getResult();
    }

    public function tareasrealizadas($registroid) {

        $em = $this->getEntityManager();

        $consulta = $em->createQuery('SELECT t FROM HotelBundle:Tarea t
                                    JOIN t.detalles d
                                    JOIN d.registro r
                                    WHERE r.id = :regid');

        $consulta->setParameter('regid', $registroid);

        return $consulta->getResult();

    }

    public function estadohabitacion($hab) {

        $em = $this->getEntityManager();

        $consulta = $em->createQuery('SELECT r FROM RegistroBundle:Registro r
                                    WHERE r.fecha = :fecha
                                    AND r.habitacion = :hab
                                    ');

        $consulta->setParameter('fecha', date("Y-m-d"));
        $consulta->setParameter('hab', $hab);

//        return $consulta->getSingleResult();
        return $consulta->getOneOrNullResult();
    }

    public function estadohabitacionfecha($hab, $fec) {

        $em = $this->getEntityManager();

        $consulta = $em->createQuery('SELECT r FROM RegistroBundle:Registro r
                                    WHERE r.fecha = :fecha
                                    AND r.habitacion = :hab
                                    AND r.comenzado=1
                                    ');

        $consulta->setParameter('fecha', date($fec));
        $consulta->setParameter('hab', $hab);

//        return $consulta->getSingleResult();
        return $consulta->getOneOrNullResult();
    }

    public function gethistoriala($hab,$usu,$fec) {
        // print_r("Usu: " . $usu . "<br />");
        // print_r("Hab: " . $hab . "<br />");
        // print_r("Fec: " . $fec . "<br />");

        if ($hab==0) { $hab=null; }

        $em = $this->getEntityManager();
        if ($usu=="0") { unset($usu); }
        if ($hab=="0") { unset($hab); }
        if (isset($hab) && !isset($usu) && ($fec=="")) {
            // print_r("bnbnbnbn");
            $consulta = $em->createQuery('SELECT r FROM RegistroBundle:Registro r
                JOIN r.habitacion h
                WHERE h.id = :hab
                AND r.fecha <= :fec
                ORDER BY r.fecha desc, h.nombre asc
            ');
            $consulta->setParameter('hab',$hab);
            $fec = new \DateTime();
            $consulta->setParameter('fec',$fec);

        } elseif (!isset($hab) && isset($usu) && ($fec=="")) {
            // print_r("mmmmmmm");
            $consulta = $em->createQuery('SELECT r FROM RegistroBundle:Registro r
                LEFT JOIN r.habitacion h
                WHERE r.usuario = :usu
                ORDER BY r.fecha desc, h.nombre asc
            ');
            // print_r( $consulta->getSql() );
            $consulta->setParameter('usu',$usu);
        } elseif (!isset($hab) && !isset($usu) && ($fec!="")) {
            // print_r("ggggggg");
            $consulta = $em->createQuery('SELECT r FROM RegistroBundle:Registro r
                JOIN r.habitacion h
                WHERE r.fecha = :fec
                ORDER BY r.fecha desc, h.nombre asc
            ');
            $consulta->setParameter('fec',date($fec));
        } elseif (isset($hab) && isset($usu) && ($fec=="")) {
            // print_r("vvvvvvv");
            $consulta = $em->createQuery('SELECT r FROM RegistroBundle:Registro r
                JOIN r.habitacion h
                JOIN r.usuario u
                WHERE h.id = :hab
                AND u.id = :usu
                AND r.fecha = :fec
                ORDER BY r.fecha desc, h.nombre asc
            ');
            $consulta->setParameter('hab',$hab);
            $consulta->setParameter('usu',$usu);
            $fec = new \DateTime();
            $consulta->setParameter('fec',$fec);
        } elseif (isset($hab) && !isset($usu) && ($fec!="")) {
            // print_r("uuuuuuuu: " . $hab);
            $consulta = $em->createQuery('SELECT r FROM RegistroBundle:Registro r
                JOIN r.habitacion h
                WHERE r.habitacion = :hab
                AND r.fecha = :fec
                ORDER BY r.fecha desc
            ');
            $consulta->setParameter('hab',$hab);
            $consulta->setParameter('fec',date($fec));
        } elseif (!isset($hab) && isset($usu) && ($fec!="")) {
            // print_r("rrrrrrrr");
            $consulta = $em->createQuery('SELECT r FROM RegistroBundle:Registro r
                JOIN r.habitacion h
                WHERE r.usuario = :usu
                AND r.fecha = :fec
                ORDER BY r.fecha desc, h.nombre asc
            ');
            $consulta->setParameter('usu',$usu);
            $consulta->setParameter('fec',date($fec));
        } elseif (isset($hab) && isset($usu) && ($fec!="")) {
            // print_r("wwwwwww");
            $consulta = $em->createQuery('SELECT r FROM RegistroBundle:Registro r
                JOIN r.habitacion h
                WHERE h.id = :hab
                AND r.usuario = :usu
                AND r.fecha = :fec
                ORDER BY r.fecha desc, h.nombre asc
            ');
            $consulta->setParameter('hab',$hab);
            $consulta->setParameter('usu',$usu);
            $consulta->setParameter('fec',date($fec));
        } else {
            // print_r("qqqqqqqqqq");
            $consulta = $em->createQuery('SELECT r FROM RegistroBundle:Registro r
                JOIN r.habitacion h

                ORDER BY r.fecha desc, h.nombre asc
            ');
            // $fec = new \DateTime();
            // $consulta->setParameter('fec',$fec);
        }

        $consulta->setMaxResults(50);
        return $consulta->getResult();

    }

}