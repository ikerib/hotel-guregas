<?php

namespace Gitek\RegistroBundle\Entity;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Gitek\RegistroBundle\Entity\Master
 *
 * @ORM\Entity()
 */
class Master
{

    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /** @ORM\OneToMany(targetEntity="Gitek\RegistroBundle\Entity\Registro", mappedBy="master", cascade={"persist"}) */
    protected $registros;

    protected $usuario;

    public function __construct()
    {
        $this->registros = new ArrayCollection();
    }

    /* IKER */
    public function getUsuario()
    {
        return $this->usuario;
    }

    // /* iker */
    public function setRegistros( $registros)
    {
        foreach ($registros as $r) {
            $r->setMaster($this);
        }

        $this->registros = $registros;
    }


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    // /**
    //  * Add registros
    //  *
    //  * @param Gitek\RegistroBundle\Entity\Registro $registros
    //  */
    // public function addRegistro(\Gitek\RegistroBundle\Entity\Registro $registros)
    // {
    //     $this->registros[] = $registros;
    // }

    /**
     * Get registros
     *
     * @return Doctrine\Common\Collections\Collection 
     */
    public function getRegistros()
    {
        return $this->registros;
    }

    /**
     * Add registros
     *
     * @param Gitek\RegistroBundle\Entity\Registro $registros
     */
    public function addRegistro(\Gitek\RegistroBundle\Entity\Registro $registros)
    {
        $this->registros[] = $registros;
    }
}