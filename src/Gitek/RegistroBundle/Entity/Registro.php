<?php

namespace Gitek\RegistroBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Gitek\RegistroBundle\Entity\Registro
 *
 * @ORM\Entity
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Gitek\RegistroBundle\Entity\RegistroRepository"))
 */
class Registro
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var date $fecha
     *
     * @ORM\Column(name="fecha", type="date")
     */
    private $fecha;

    /**
     * @var smallint $comenzado
     *
     * @ORM\Column(name="comenzado", type="smallint", nullable=true)
     */
    private $comenzado=0;

    /**
     * @var smallint $completado
     *
     * @ORM\Column(name="completado", type="smallint", nullable=true)
     */
    private $completado=0;

    /**
     * @var smallint $ocupado
     *
     * @ORM\Column(name="ocupado", type="smallint", nullable=true)
     */
    private $ocupado=0;

   /**
     * @var datetime $created_at
     *
     * @ORM\Column(name="created_at", type="datetime")
     */
    private $created_at;

    /**
     * @var datetime $updated_at
     *
     * @ORM\Column(name="updated_at", type="datetime")
     */
    private $updated_at;

    /** @ORM\OneToMany(targetEntity="Gitek\RegistroBundle\Entity\Registrodet", mappedBy="registro") */
    protected $detalles;

    /** @ORM\ManyToOne(targetEntity="Gitek\UsuarioBundle\Entity\Usuario") */
    protected $usuario;

    /** @ORM\ManyToOne(targetEntity="Gitek\HotelBundle\Entity\Tipotarea") */
    protected $tipotarea;

    /** @ORM\ManyToOne(targetEntity="Gitek\HotelBundle\Entity\Habitacion") */
    protected $habitacion;

    /** @ORM\ManyToOne(targetEntity="Gitek\RegistroBundle\Entity\Master", inversedBy="registros", cascade={"persist"}))) */
    protected $master;

    /**
     * @ORM\ManyToMany(targetEntity="Gitek\HotelBundle\Entity\Incidencia", inversedBy="registros")
     * @ORM\JoinTable(name="incidencia_registro",
     *      joinColumns={@ORM\JoinColumn(name="registro_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="incidencia_id", referencedColumnName="id")}
     * )
     */
    protected $incidencias;

    public function __construct()
    {
        $this->created_at = new \DateTime();
        $this->updated_at = new \DateTime();
    }

    // public function __toString()
    // {
    //     return $this->getNombre();
    // }




    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set fecha
     *
     * @param date $fecha
     */
    public function setFecha($fecha)
    {
        $this->fecha = $fecha;
    }

    /**
     * Get fecha
     *
     * @return date
     */
    public function getFecha()
    {
        return $this->fecha;
    }

    /**
     * Set comenzado
     *
     * @param smallint $comenzado
     */
    public function setComenzado($comenzado)
    {
        $this->comenzado = $comenzado;
    }

    /**
     * Get comenzado
     *
     * @return smallint
     */
    public function getComenzado()
    {
        return $this->comenzado;
    }

    /**
     * Set completado
     *
     * @param smallint $completado
     */
    public function setCompletado($completado)
    {
        $this->completado = $completado;
    }

    /**
     * Get completado
     *
     * @return smallint
     */
    public function getCompletado()
    {
        return $this->completado;
    }

    /**
     * Set ocupado
     *
     * @param smallint $ocupado
     */
    public function setOcupado($ocupado)
    {
        $this->ocupado = $ocupado;
    }

    /**
     * Get ocupado
     *
     * @return smallint
     */
    public function getOcupado()
    {
        return $this->ocupado;
    }

    /**
     * Set created_at
     *
     * @param datetime $createdAt
     */
    public function setCreatedAt($createdAt)
    {
        $this->created_at = $createdAt;
    }

    /**
     * Get created_at
     *
     * @return datetime
     */
    public function getCreatedAt()
    {
        return $this->created_at;
    }

    /**
     * Set updated_at
     *
     * @param datetime $updatedAt
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updated_at = $updatedAt;
    }

    /**
     * Get updated_at
     *
     * @return datetime
     */
    public function getUpdatedAt()
    {
        return $this->updated_at;
    }

    /**
     * Set usuario
     *
     * @param Gitek\UsuarioBundle\Entity\Usuario $usuario
     */
    public function setUsuario(\Gitek\UsuarioBundle\Entity\Usuario $usuario)
    {
        $this->usuario = $usuario;
    }

    /**
     * Get usuario
     *
     * @return Gitek\UsuarioBundle\Entity\Usuario
     */
    public function getUsuario()
    {
        return $this->usuario;
    }

    /**
     * Set tipotarea
     *
     * @param Gitek\HotelBundle\Entity\Tipotarea $tipotarea
     */
    public function setTipotarea(\Gitek\HotelBundle\Entity\Tipotarea $tipotarea)
    {
        $this->tipotarea = $tipotarea;
    }

    /**
     * Get tipotarea
     *
     * @return Gitek\HotelBundle\Entity\Tipotarea
     */
    public function getTipotarea()
    {
        return $this->tipotarea;
    }

    /**
     * Set habitacion
     *
     * @param Gitek\HotelBundle\Entity\Habitacion $habitacion
     */
    public function setHabitacion(\Gitek\HotelBundle\Entity\Habitacion $habitacion)
    {
        $this->habitacion = $habitacion;
    }

    /**
     * Get habitacion
     *
     * @return Gitek\HotelBundle\Entity\Habitacion
     */
    public function getHabitacion()
    {
        return $this->habitacion;
    }

    /**
     * Add incidencias
     *
     * @param Gitek\HotelBundle\Entity\Incidencia $incidencias
     */
    public function addIncidencia(\Gitek\HotelBundle\Entity\Incidencia $incidencias)
    {
        $this->incidencias[] = $incidencias;
    }

    /**
     * Get incidencias
     *
     * @return Doctrine\Common\Collections\Collection
     */
    public function getIncidencias()
    {
        return $this->incidencias;
    }

    /**
     * Set master
     *
     * @param Gitek\RegistroBundle\Entity\Master $master
     */
    public function setMaster(\Gitek\RegistroBundle\Entity\Master $master)
    {
        $this->master = $master;
    }

    /**
     * Get master
     *
     * @return Gitek\RegistroBundle\Entity\Master
     */
    public function getMaster()
    {
        return $this->master;
    }

    /**
     * Add detalles
     *
     * @param Gitek\RegistroBundle\Entity\Registrodet $detalles
     */
    public function addRegistrodet(\Gitek\RegistroBundle\Entity\Registrodet $detalles)
    {
        $this->detalles[] = $detalles;
    }

    /**
     * Get detalles
     *
     * @return Doctrine\Common\Collections\Collection
     */
    public function getDetalles()
    {
        return $this->detalles;
    }
}