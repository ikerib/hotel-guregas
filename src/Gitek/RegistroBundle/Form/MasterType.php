<?php

namespace Gitek\RegistroBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Doctrine\ORM\EntityRepository;

class MasterType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $builder
            ->add('usuario',
                'entity',
                array(
                    'class' => 'Gitek\\UsuarioBundle\\Entity\\Usuario',
                    'empty_value' => 'Selecciona una usuario',
                    'query_builder' => function(EntityRepository $repositorio)
                    {
                        return $repositorio->createQueryBuilder('c')
                                ->innerJoin('c.tipousuario','t')
                                ->where("t.id = :tipoa ")
                                ->orderBy('c.apellidos,c.nombre', 'ASC')
                                ->setParameter('tipoa', 3);
                    },
                )
            )
            ->add('registros', 'collection', array(
                    'type' => new RegistroType(),
                    'allow_add' => true,
                    'by_reference' => false,
                ));
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
                'data_class' => 'Gitek\RegistroBundle\Entity\Master',
            ));
    }


    public function getName()
    {
        return 'master';
    }
}