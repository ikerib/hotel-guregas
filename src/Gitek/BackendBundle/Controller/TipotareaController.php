<?php

namespace Gitek\BackendBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Gitek\HotelBundle\Entity\Tipotarea;
use Gitek\BackendBundle\Form\TipotareaType;

/**
 * Tipotarea controller.
 *
 */
class TipotareaController extends Controller
{
    /**
     * Lists all Tipotarea entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('HotelBundle:Tipotarea')->findAll();

        return $this->render('BackendBundle:Tipotarea:index.html.twig', array(
            'entities' => $entities
        ));
    }

    /**
     * Finds and displays a Tipotarea entity.
     *
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('HotelBundle:Tipotarea')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Tipotarea entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return $this->render('BackendBundle:Tipotarea:show.html.twig', array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to create a new Tipotarea entity.
     *
     */
    public function newAction()
    {
        $entity = new Tipotarea();
        $form   = $this->createForm(new TipotareaType(), $entity);

        return $this->render('BackendBundle:Tipotarea:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView()
        ));
    }

    /**
     * Creates a new Tipotarea entity.
     *
     */
    public function createAction()
    {
        $entity  = new Tipotarea();
        $request = $this->getRequest();
        $form    = $this->createForm(new TipotareaType(), $entity);
        $form->bind($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('tipotarea_show', array('id' => $entity->getId())));

        }

        return $this->render('BackendBundle:Tipotarea:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView()
        ));
    }

    /**
     * Displays a form to edit an existing Tipotarea entity.
     *
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('HotelBundle:Tipotarea')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Tipotarea entity.');
        }

        $editForm = $this->createForm(new TipotareaType(), $entity);
        $deleteForm = $this->createDeleteForm($id);

        return $this->render('BackendBundle:Tipotarea:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Edits an existing Tipotarea entity.
     *
     */
    public function updateAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('HotelBundle:Tipotarea')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Tipotarea entity.');
        }

        $editForm   = $this->createForm(new TipotareaType(), $entity);
        $deleteForm = $this->createDeleteForm($id);

        $request = $this->getRequest();

        if ('POST' === $request->getMethod()) {
            $editForm->bind($request);
//            if ($editForm->isValid()) {

            $em->persist($entity);
            $em->flush();
//
//            return $this->redirect($this->generateUrl('tipotarea_edit', array('id' => $id)));
//            } else {

//                $errors = $editForm->getErrors();
//                // iterate on it
//                foreach( $errors as $error )
//                {
//                    // Do stuff with:
//                    print_r($error);
//                    print_r("<br />");
//                }
//            }
        }

        return $this->render('BackendBundle:Tipotarea:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a Tipotarea entity.
     *
     */
    public function deleteAction($id)
    {
        $form = $this->createDeleteForm($id);
        $request = $this->getRequest();

        $form->bind($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('HotelBundle:Tipotarea')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find Tipotarea entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('tipotarea'));
    }

    private function createDeleteForm($id)
    {
        return $this->createFormBuilder(array('id' => $id))
            ->add('id', 'hidden')
            ->getForm()
        ;
    }
}
