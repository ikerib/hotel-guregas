<?php

namespace Gitek\BackendBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Gitek\UsuarioBundle\Entity\Tipousuario;
use Gitek\BackendBundle\Form\TipousuarioType;

/**
 * Tipousuario controller.
 *
 */
class TipousuarioController extends Controller
{
    /**
     * Lists all Tipousuario entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('UsuarioBundle:Tipousuario')->findAll();

        return $this->render('BackendBundle:Tipousuario:index.html.twig', array(
            'entities' => $entities
        ));
    }

    /**
     * Finds and displays a Tipousuario entity.
     *
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('UsuarioBundle:Tipousuario')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Tipousuario entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return $this->render('BackendBundle:Tipousuario:show.html.twig', array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),

        ));
    }

    /**
     * Displays a form to create a new Tipousuario entity.
     *
     */
    public function newAction()
    {
        $entity = new Tipousuario();
        $form   = $this->createForm(new TipousuarioType(), $entity);

        return $this->render('BackendBundle:Tipousuario:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView()
        ));
    }

    /**
     * Creates a new Tipousuario entity.
     *
     */
    public function createAction()
    {
        $entity  = new Tipousuario();
        $request = $this->getRequest();
        $form    = $this->createForm(new TipousuarioType(), $entity);
        $form->bind($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('backend_tipousuario_show', array('id' => $entity->getId())));
            
        }

        return $this->render('BackendBundle:Tipousuario:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView()
        ));
    }

    /**
     * Displays a form to edit an existing Tipousuario entity.
     *
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('UsuarioBundle:Tipousuario')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Tipousuario entity.');
        }

        $editForm = $this->createForm(new TipousuarioType(), $entity);
        $deleteForm = $this->createDeleteForm($id);

        return $this->render('BackendBundle:Tipousuario:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Edits an existing Tipousuario entity.
     *
     */
    public function updateAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('UsuarioBundle:Tipousuario')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Tipousuario entity.');
        }

        $editForm   = $this->createForm(new TipousuarioType(), $entity);
        $deleteForm = $this->createDeleteForm($id);

        $request = $this->getRequest();

        $editForm->bind($request);

        if ($editForm->isValid()) {
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('backend_tipousuario_edit', array('id' => $id)));
        }

        return $this->render('BackendBundle:Tipousuario:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a Tipousuario entity.
     *
     */
    public function deleteAction($id)
    {
        $form = $this->createDeleteForm($id);
        $request = $this->getRequest();

        $form->bind($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('UsuarioBundle:Tipousuario')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find Tipousuario entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('backend_tipousuario'));
    }

    private function createDeleteForm($id)
    {
        return $this->createFormBuilder(array('id' => $id))
            ->add('id', 'hidden')
            ->getForm()
        ;
    }
}
