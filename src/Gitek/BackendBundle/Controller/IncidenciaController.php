<?php

namespace Gitek\BackendBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Gitek\HotelBundle\Entity\Incidencia;
use Gitek\BackendBundle\Form\IncidenciaType;

/**
 * Incidencia controller.
 *
 */
class IncidenciaController extends Controller
{
    /**
     * Lists all Incidencia entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('HotelBundle:Incidencia')->findAll();

        return $this->render('BackendBundle:Incidencia:index.html.twig', array(
            'entities' => $entities
        ));
    }

    /**
     * Finds and displays a Incidencia entity.
     *
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('HotelBundle:Incidencia')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Incidencia entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return $this->render('BackendBundle:Incidencia:show.html.twig', array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to create a new Incidencia entity.
     *
     */
    public function newAction()
    {
        $entity = new Incidencia();
        $form   = $this->createForm(new IncidenciaType(), $entity);

        return $this->render('BackendBundle:Incidencia:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView()
        ));
    }

    /**
     * Creates a new Incidencia entity.
     *
     */
    public function createAction()
    {
        $entity  = new Incidencia();
        $request = $this->getRequest();
        $form    = $this->createForm(new IncidenciaType(), $entity);
        $form->bind($request);

        if ($form->isValid()) {

            // Copiar la foto subida y guardar la ruta
//            $entity->subirImagen($this->container->getParameter('gitek.directorio.imagenes'));

            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('incidencia_show', array('id' => $entity->getId())));

        }

        return $this->render('BackendBundle:Incidencia:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView()
        ));
    }

    /**
     * Displays a form to edit an existing Incidencia entity.
     *
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('HotelBundle:Incidencia')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Incidencia entity.');
        }

        $editForm = $this->createForm(new IncidenciaType(), $entity);
        $deleteForm = $this->createDeleteForm($id);

        return $this->render('BackendBundle:Incidencia:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Edits an existing Incidencia entity.
     *
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('HotelBundle:Incidencia')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Incidencia entity.');
        }

        $editForm   = $this->createForm(new IncidenciaType(), $entity);
        $deleteForm = $this->createDeleteForm($id);
        $editForm->bind($request);

        if ($editForm->isValid()) {
            $entity->setUpdatedAt(new \DateTime());
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('incidencia_edit', array('id' => $id)));
        }

        return $this->render('BackendBundle:Incidencia:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a Incidencia entity.
     *
     */
    public function deleteAction($id)
    {
        $form = $this->createDeleteForm($id);
        $request = $this->getRequest();

        $form->bind($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('HotelBundle:Incidencia')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find Incidencia entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('incidencia'));
    }

    private function createDeleteForm($id)
    {
        return $this->createFormBuilder(array('id' => $id))
            ->add('id', 'hidden')
            ->getForm()
        ;
    }
}
