<?php

namespace Gitek\BackendBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Gitek\HotelBundle\Entity\Habitacion;
use Gitek\BackendBundle\Form\HabitacionType;

/**
 * Habitacion controller.
 *
 */
class HabitacionController extends Controller
{
    /**
     * Lists all Habitacion entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('HotelBundle:Habitacion')->findAll();

        return $this->render('BackendBundle:Habitacion:index.html.twig', array(
            'entities' => $entities
        ));
    }

    /**
     * Finds and displays a Habitacion entity.
     *
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('HotelBundle:Habitacion')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Habitacion entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return $this->render('BackendBundle:Habitacion:show.html.twig', array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),

        ));
    }

    /**
     * Displays a form to create a new Habitacion entity.
     *
     */
    public function newAction()
    {
        $entity = new Habitacion();
        $form   = $this->createForm(new HabitacionType(), $entity);

        return $this->render('BackendBundle:Habitacion:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView()
        ));
    }

    /**
     * Creates a new Habitacion entity.
     *
     */
    public function createAction()
    {
        $entity  = new Habitacion();
        $request = $this->getRequest();
        $form    = $this->createForm(new HabitacionType(), $entity);
        $form->bind($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('habitacion_show', array('id' => $entity->getId())));

        }

        return $this->render('BackendBundle:Habitacion:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView()
        ));
    }

    /**
     * Displays a form to edit an existing Habitacion entity.
     *
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('HotelBundle:Habitacion')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Habitacion entity.');
        }

        $editForm = $this->createForm(new HabitacionType(), $entity);
        $deleteForm = $this->createDeleteForm($id);

        return $this->render('BackendBundle:Habitacion:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Edits an existing Habitacion entity.
     *
     */
    public function updateAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('HotelBundle:Habitacion')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Habitacion entity.');
        }

        $editForm   = $this->createForm(new HabitacionType(), $entity);
        $deleteForm = $this->createDeleteForm($id);

        $request = $this->getRequest();

        $editForm->bind($request);

        if ($editForm->isValid()) {
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('habitacion_edit', array('id' => $id)));
        }

        return $this->render('BackendBundle:Habitacion:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a Habitacion entity.
     *
     */
    public function deleteAction($id)
    {
        $form = $this->createDeleteForm($id);
        $request = $this->getRequest();

        $form->bind($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('HotelBundle:Habitacion')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find Habitacion entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('habitacion'));
    }

    private function createDeleteForm($id)
    {
        return $this->createFormBuilder(array('id' => $id))
            ->add('id', 'hidden')
            ->getForm()
        ;
    }
}
