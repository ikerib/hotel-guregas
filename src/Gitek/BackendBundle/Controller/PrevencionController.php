<?php

namespace Gitek\BackendBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Gitek\HotelBundle\Entity\Prevencion;
use Gitek\BackendBundle\Form\PrevencionType;

/**
 * Prevencion controller.
 *
 */
class PrevencionController extends Controller
{
    /**
     * Lists all Prevencion entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('HotelBundle:Prevencion')->findAll();

        return $this->render('BackendBundle:Prevencion:index.html.twig', array(
            'entities' => $entities
        ));
    }

    /**
     * Finds and displays a Prevencion entity.
     *
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('HotelBundle:Prevencion')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Prevencion entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return $this->render('BackendBundle:Prevencion:show.html.twig', array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),

        ));
    }

    /**
     * Displays a form to create a new Prevencion entity.
     *
     */
    public function newAction()
    {
        $entity = new Prevencion();
        $form   = $this->createForm(new PrevencionType(), $entity);

        return $this->render('BackendBundle:Prevencion:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView()
        ));
    }

    /**
     * Creates a new Prevencion entity.
     *
     */
    public function createAction()
    {
        $entity  = new Prevencion();
        $request = $this->getRequest();
        $form    = $this->createForm(new PrevencionType(), $entity);
        $form->bind($request);

        if ($form->isValid()) {

            // Copiar la foto subida y guardar la ruta
//            $entity->subirImagen($this->container->getParameter('gitek.directorio.imagenes'));

            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('prevencion_show', array('id' => $entity->getId())));

        }

        return $this->render('BackendBundle:Prevencion:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView()
        ));
    }

    /**
     * Displays a form to edit an existing Prevencion entity.
     *
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('HotelBundle:Prevencion')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Prevencion entity.');
        }

        $editForm = $this->createForm(new PrevencionType(), $entity);
        $deleteForm = $this->createDeleteForm($id);

        return $this->render('BackendBundle:Prevencion:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Edits an existing Prevencion entity.
     *
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('HotelBundle:Prevencion')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Prevencion entity.');
        }

        $editForm   = $this->createForm(new PrevencionType(), $entity);
        $deleteForm = $this->createDeleteForm($id);
        $editForm->bind($request);

        if ($editForm->isValid()) {
            $entity->setUpdatedAt(new \DateTime());
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('prevencion_edit', array('id' => $id)));
        }

        return $this->render('BackendBundle:Prevencion:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a Prevencion entity.
     *
     */
    public function deleteAction($id)
    {
        $form = $this->createDeleteForm($id);
        $request = $this->getRequest();

        $form->bind($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('HotelBundle:Prevencion')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find Prevencion entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('prevencion'));
    }

    private function createDeleteForm($id)
    {
        return $this->createFormBuilder(array('id' => $id))
            ->add('id', 'hidden')
            ->getForm()
        ;
    }
}
