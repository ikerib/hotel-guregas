<?php

namespace Gitek\BackendBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;

class UsuarioType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('username')
            ->add('usernameCanonical')
            ->add('nombre')
            ->add('apellidos')
            ->add('email')
            ->add('password')
//            ->add('salt')
            ->add('file')
            ->add('tipousuario')
            // ->add('tipousuario','collection', array('type' => new TipousuarioType())
        ;
    }

    public function getName()
    {
        return 'gitek_backendbundle_usuariotype';
    }
}
