<?php

namespace Gitek\BackendBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class MaterialType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('nombre')
            ->add('image', 'file', array(
              'data_class' => 'Symfony\Component\HttpFoundation\File\File',
              'required' => false,
            ))
        ;
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
                'data_class' => 'Gitek\HotelBundle\Entity\Material',
            ));
    }

    public function getName()
    {
        return 'gitek_hotelbundle_materialtype';
    }
}
