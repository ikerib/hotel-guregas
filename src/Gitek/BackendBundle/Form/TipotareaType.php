<?php

namespace Gitek\BackendBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;

class TipotareaType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('nombre')
            ->add('tareas', 'entity', array(
                'class' => 'HotelBundle:Tarea',
                'multiple' => true,
                'label' => 'Tareas:'
            ))
        ;
    }

    public function getName()
    {
        return 'gitek_hotelbundle_tipotareatype';
    }
}
