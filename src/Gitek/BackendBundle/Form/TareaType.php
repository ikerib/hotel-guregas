<?php

namespace Gitek\BackendBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class TareaType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('nombre')
            ->add('orden')
            ->add('image', 'file', array(
              'data_class' => 'Symfony\Component\HttpFoundation\File\File',
              'required' => false,
            ))
            ->add('video')
            ->add('materiales', 'entity', array(
                'class' => 'HotelBundle:Material',
                'multiple' => true,
                'label' => 'Materiales:'
            ))
            ->add('prevenciones', 'entity', array(
                'class' => 'HotelBundle:Prevencion',
                'multiple' => true,
                'label' => 'EPIs:'
            ))
            ;
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
                'data_class' => 'Gitek\HotelBundle\Entity\Tarea',
            ));
    }

    public function getName()
    {
        return 'gitek_hotelbundle_tareatype';
    }
}
