<?php

namespace Gitek\UsuarioBundle\Form\Type;

use Symfony\Component\Form\FormBuilderInterface;
use FOS\UserBundle\Form\Type\RegistrationFormType as BaseType;

class RegistrationFormType extends BaseType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        parent::buildForm($builder, $options);

        // add your custom field
        $builder->add('nombre');
        $builder->add('apellidos');
        $builder->add('tipousuario');
        $builder->add('file');
    }

    public function getName()
    {
        return 'hotel_user_registration';
    }
}
