// <?php

// namespace Gitek\UsuarioBundle\DataFixtures\ORM;

// use Doctrine\Common\Persistence\ObjectManager; 
// use Doctrine\Common\DataFixtures\AbstractFixture;
// use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
// use Gitek\UsuarioBundle\Entity\Usuario;

// class UsuarioFixtures extends AbstractFixture implements OrderedFixtureInterface
// {
//     public function load(ObjectManager $manager)
//     {
//         for ($i=1; $i<=10; $i++) {
//             $usuario = new Usuario();
            
//             $usuario->setNombre($this->getNombre());
//             $usuario->setApellidos($this->getApellidos());
//             $usuario->setEmail('usuario'.$i.'@localhost.com');
            
//             $usuario->setSalt(md5(time()));
            
//             $passwordEnClaro = 'usuario'.$i;
//             // $encoder = $this->container->get('security.encoder_factory')->getEncoder($usuario);
//             // $passwordCodificado = $encoder->encodePassword($passwordEnClaro, $usuario->getSalt());
//             $usuario->setPassword($passwordEnClaro);
            
//             if (rand() % 2) {
//                $usuario->setTipousuario($manager->merge($this->getReference('tipousuario_limpieza')));
//             }
//             else {
//                 $usuario->setTipousuario($manager->merge($this->getReference('tipousuario_encargado')));
//             }
            
//             $manager->persist($usuario); 
//         }

//         //usuario Administrador

//         $usuario = new Usuario();
            
//         $usuario->setNombre("Iker");
//         $usuario->setApellidos("Ibarguren");
//         $usuario->setEmail('iibarguren@grupogureak.com');
        
//         $usuario->setSalt(md5(time()));
        
//         $passwordEnClaro = "!Admin1234";
//         // $encoder = $this->container->get('security.encoder_factory')->getEncoder($usuario);
//         // $passwordCodificado = $encoder->encodePassword($passwordEnClaro, $usuario->getSalt());
//         $usuario->setPassword($passwordEnClaro);
        
//         $usuario->setTipousuario($manager->merge($this->getReference('tipousuario_admin')));
        
//         $manager->persist($usuario); 

//         $manager->flush();
//     }

//     public function getOrder()
//     {
//         return 10; 
//     }


//     /**
//      * Generador aleatorio de nombres de personas
//      * Aproximadamente genera un 50% de hombres y un 50% de mujeres
//      */
//     private function getNombre()
//     {
//         // Los nombres más populares en España según el INE
//         // Fuente: http://www.ine.es/daco/daco42/nombyapel/nombyapel.htm
        
//         $hombres = array('Antonio', 'José', 'Manuel', 'Francisco', 'Juan', 'David', 'José Antonio', 'José Luis', 'Jesús', 'Javier', 'Francisco Javier', 'Carlos', 'Daniel', 'Miguel', 'Rafael', 'Pedro', 'José Manuel', 'Ángel', 'Alejandro', 'Miguel Ángel', 'José María', 'Fernando', 'Luis', 'Sergio', 'Pablo', 'Jorge', 'Alberto');
//         $mujeres = array('María Carmen', 'María', 'Carmen', 'Josefa', 'Isabel', 'Ana María', 'María Dolores', 'María Pilar', 'María Teresa', 'Ana', 'Francisca', 'Laura', 'Antonia', 'Dolores', 'María Angeles', 'Cristina', 'Marta', 'María José', 'María Isabel', 'Pilar', 'María Luisa', 'Concepción', 'Lucía', 'Mercedes', 'Manuela', 'Elena', 'Rosa María');
        
//         if (rand() % 2) {
//             return $hombres[rand(0, count($hombres)-1)];
//         }
//         else {
//             return $mujeres[rand(0, count($mujeres)-1)];
//         }
//     }
    
//     /**
//      * Generador aleatorio de apellidos de personas
//      */
//     private function getApellidos()
//     {
//         // Los apellidos más populares en España según el INE
//         // Fuente: http://www.ine.es/daco/daco42/nombyapel/nombyapel.htm
        
//         $apellidos = array('García', 'González', 'Rodríguez', 'Fernández', 'López', 'Martínez', 'Sánchez', 'Pérez', 'Gómez', 'Martín', 'Jiménez', 'Ruiz', 'Hernández', 'Díaz', 'Moreno', 'Álvarez', 'Muñoz', 'Romero', 'Alonso', 'Gutiérrez', 'Navarro', 'Torres', 'Domínguez', 'Vázquez', 'Ramos', 'Gil', 'Ramírez', 'Serrano', 'Blanco', 'Suárez', 'Molina', 'Morales', 'Ortega', 'Delgado', 'Castro', 'Ortíz', 'Rubio', 'Marín', 'Sanz', 'Iglesias', 'Nuñez', 'Medina', 'Garrido');
        
//         return $apellidos[rand(0, count($apellidos)-1)].' '.$apellidos[rand(0, count($apellidos)-1)];
//     }
// }