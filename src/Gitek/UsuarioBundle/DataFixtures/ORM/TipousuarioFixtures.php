<?php

namespace Gitek\UsuarioBundle\DataFixtures\ORM;

use Doctrine\Common\Persistence\ObjectManager; 
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface; 
use Gitek\UsuarioBundle\Entity\Tipousuario;

class TipousuarioFixtures extends AbstractFixture implements OrderedFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $tipousuario = new Tipousuario();
        $tipousuario->setNombre("Recepcion");
        $manager->persist($tipousuario);   
        $this->addReference('tipousuario_admin', $tipousuario);
        
        $tipousuario = new Tipousuario();
        $tipousuario->setNombre("Encargado");
        $manager->persist($tipousuario);   
        $this->addReference('tipousuario_limpieza', $tipousuario);

        $tipousuario = new Tipousuario();
        $tipousuario->setNombre("Servicio limpieza");
        $manager->persist($tipousuario);   
        $this->addReference('tipousuario_encargado', $tipousuario);

        $manager->flush();

        
    }

    public function getOrder()
    {
        return 9; 
    }
}