<?php

namespace Gitek\UsuarioBundle\Entity;

use Doctrine\ORM\EntityRepository;

class UsuarioRepository extends EntityRepository
{

    public function bilatuLangileak() {
        $em = $this->getEntityManager();

        $consulta = $em->createQuery('SELECT u FROM UsuarioBundle:Usuario u JOIN u.tipousuario t
                                    WHERE t.id = :tipoa
                                    AND u.enabled=1
                                    ORDER BY u.apellidos, u.nombre ASC');
        $consulta->setParameter('tipoa', '3');
        $consulta->useResultCache(true, 600);

        return $consulta->getResult();
    }
}