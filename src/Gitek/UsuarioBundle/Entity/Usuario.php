<?php

namespace Gitek\UsuarioBundle\Entity;

//use FOS\UserBundle\Entity\User as BaseUser;
use FOS\UserBundle\Model\User as BaseUser;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Bridge\Doctrine\Validator\Constraints as DoctrineAssert;
use Vich\UploaderBundle\Mapping\Annotation as Vich;

/**
 * Gitek\UsuarioBundle\Entity\Usuario
 *
 * @ORM\Table()
 * @ORM\Entity()
 * @ORM\HasLifecycleCallbacks
 * @Vich\Uploadable
 * @ORM\Entity(repositoryClass="Gitek\UsuarioBundle\Entity\UsuarioRepository")
 */
class Usuario extends BaseUser
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string $nombre
     *
     * @ORM\Column(name="nombre", type="string", length=255, nullable=true)
     * @Assert\NotBlank(message="Introduce tu nombre.", groups={"Registration", "Profile"})
     * @Assert\Length(
     *      min = "3",
     *      max = "255",
     *      minMessage = "Your first name must be at least {{ limit }} characters length",
     *      maxMessage = "Your first name cannot be longer than than {{ limit }} characters length",
     *      groups={"Registration", "Profile"}
     * )
     */
     // * @Assert\MinLength(limit="3", message="The nombre is too short.", groups={"Registration", "Profile"})
     // * @Assert\MaxLength(limit="255", message="The nombre is too long.", groups={"Registration", "Profile"})
    protected $nombre;

    /**
     * @var string $apellidos
     *
     * @ORM\Column(name="apellidos", type="string", length=255, nullable=true)
     * @Assert\NotBlank(message="Introduce tu apellido.", groups={"Registration", "Profile"})
     * @Assert\Length(
     *      min = "3",
     *      max = "255",
     *      minMessage = "Your first name must be at least {{ limit }} characters length",
     *      maxMessage = "Your first name cannot be longer than than {{ limit }} characters length",
     *      groups={"Registration", "Profile"}
     * )
     */
     // * @Assert\MinLength(limit="3", message="The nombre is too short.", groups={"Registration", "Profile"})
     // * @Assert\MaxLength(limit="255", message="The nombre is too long.", groups={"Registration", "Profile"})
    protected $apellidos;

   /**
    * @ORM\Column(type="datetime", name="created_at")
    *
    * @var DateTime $createdAt
    */
    protected $createdAt;

    /**
     * @ORM\Column(type="datetime", name="updated_at", nullable=true)
     *
     * @var DateTime $updatedAt
     */
    protected $updatedAt;

    /** @ORM\ManyToOne(targetEntity="Gitek\UsuarioBundle\Entity\Tipousuario") */
    protected $tipousuario;

    public function __construct()
    {
        parent::__construct();
        $this->createdAt = new \DateTime();
        $this->updatedAt = new \DateTime();
    }

    public function __toString()
    {
        return $this->getNombre().' '.$this->getApellidos();
    }

    /**
     * @Assert\File(
     *     maxSize="1M",
     *     mimeTypes={"image/png", "image/jpeg", "image/pjpeg"}
     * )
     * @Vich\UploadableField(mapping="uploads", fileNameProperty="path", nullable=true)
     *
     * @var File $image
     */
    private $file;


    /**
     * @var string $path
     *
     * @ORM\Column(name="path", type="string", length=255, nullable=true)
     */
    private $path;

    public function setFile($file) {
        $this -> file = $file;
        if ($file instanceof UploadedFile) {
            $this->setUpdatedAt(new \DateTime());
        }
    }

    public function getFile() {
        return $this->file;
    }




    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nombre
     *
     * @param string $nombre
     * @return Usuario
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;

        return $this;
    }

    /**
     * Get nombre
     *
     * @return string
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * Set apellidos
     *
     * @param string $apellidos
     * @return Usuario
     */
    public function setApellidos($apellidos)
    {
        $this->apellidos = $apellidos;

        return $this;
    }

    /**
     * Get apellidos
     *
     * @return string
     */
    public function getApellidos()
    {
        return $this->apellidos;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     * @return Usuario
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     * @return Usuario
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * Set path
     *
     * @param string $path
     * @return Usuario
     */
    public function setPath($path)
    {
        $this->path = $path;

        return $this;
    }

    /**
     * Get path
     *
     * @return string
     */
    public function getPath()
    {
        return $this->path;
    }

    /**
     * Set tipousuario
     *
     * @param \Gitek\UsuarioBundle\Entity\Tipousuario $tipousuario
     * @return Usuario
     */
    public function setTipousuario(\Gitek\UsuarioBundle\Entity\Tipousuario $tipousuario = null)
    {
        $this->tipousuario = $tipousuario;

        return $this;
    }

    /**
     * Get tipousuario
     *
     * @return \Gitek\UsuarioBundle\Entity\Tipousuario
     */
    public function getTipousuario()
    {
        return $this->tipousuario;
    }
}