<?php

// namespace Gitek\UsuarioBundle\Controller;
namespace Gitek\UsuarioBundle;

// use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpKernel\Bundle\Bundle;

class DefaultController extends Bundle
{

    public function getParent()
    {
        return 'FOSUserBundle';
    }
}
