//configuramos el calendario
$.datepicker.regional['es'] = {
    closeText: 'Cerrar',
    prevText: '&#x3c;Ant',
    nextText: 'Sig&#x3e;',
    currentText: 'Hoy',
    monthNames: ['Enero','Febrero','Marzo','Abril','Mayo','Junio',
'               Julio','Agosto','Septiembre','Octubre','Noviembre','Diciembre'],
    monthNamesShort: ['Ene','Feb','Mar','Abr','May','Jun',
                        'Jul','Ago','Sep','Oct','Nov','Dic'],
    dayNames: ['Domingo','Lunes','Martes','Mi&eacute;rcoles','Jueves','Viernes','S&aacute;bado'],
    dayNamesShort: ['Dom','Lun','Mar','Mi&eacute;','Juv','Vie','S&aacute;b'],
    dayNamesMin: ['Do','Lu','Ma','Mi','Ju','Vi','S&aacute;'],
    weekHeader: 'Sm',
    dateFormat: 'yy-mm-dd',
    firstDay: 1,
    isRTL: false,
    showMonthAfterYear: false,
    yearSuffix: ''};

$.datepicker.setDefaults($.datepicker.regional['es']);

var now = new Date();
var y = now.getFullYear();
var m = parseInt( now.getMonth() ) + 1;
var d = now.getDate();

if ( $('#fetxa').val() ) {

} else {

    var now = new Date();
    var y = now.getFullYear();
    var m = parseInt(now.getMonth())+1;
    var d = now.getDate();
    $('#fetxa').val( y +"-" + m + "-" + d);
}

$("#datepicker").datepicker({
    dateFormat: 'yy-mm-dd',
    onSelect: function(dateText, inst) {
        $("form input.somedatefield").val(dateText);
        $("#q").val(dateText);
        $("#miq").val(dateText);
        $('#bilatu').submit();
    }
});

if( $('#fetxa').val() ) {

    queryDate = $('#fetxa').val();

    var parsedDate = $.datepicker.parseDate('yy-mm-dd', queryDate);

    $('#datepicker').datepicker('setDate', parsedDate);
}

$( init );
function init() {
    $('#img_sale').draggable({
        appendTo: "body",
        helper: "clone"
    });
    $('#img_entra').draggable({
        appendTo: "body",
        helper: "clone"
    });

    $('#hotel_gela1').droppable({
        drop: handleDropEvent
    });
    $('#hotel_gela2').droppable({
        drop: handleDropEvent
    });
    $('#hotel_gela3').droppable({
        drop: handleDropEvent
    });
    $('#hotel_gela4').droppable({
        drop: handleDropEvent
    });
    $('#hotel_gela5').droppable({
        drop: handleDropEvent
    });
    $('#hotel_gela6').droppable({
        drop: handleDropEvent
    });
    $('#hotel_gela7').droppable({
        drop: handleDropEvent
    });
    $('#hotel_gela8').droppable({
        drop: handleDropEvent
    });
    $('#hotel_gela9').droppable({
        drop: handleDropEvent
    });
    $('#hotel_gela10').droppable({
        drop: handleDropEvent
    });
}

function handleDropEvent( event, ui, hab ) {

    var draggable = ui.draggable;
    var miid = draggable.attr('id');
    var miimg = $(this).children().children();
    var mihab = $(this).data("habitacion"); // nº habitacion
    var accion="";

    // Okupazioa bada aldatzen dena, data-ocupado aldatu behar da
    if ( miid === "img_entra"){
        $(this).data("ocupado",1);
        accion="entra";
        miid = $(this).data("tarea");
    } else if ( miid === "img_sale" ) {
        $(this).data("ocupado",0);
        accion="sale";
        miid = $(this).data("tarea");
    }

    var miocu = $(this).data("ocupado");
    var mirel = $(this).data("habitacion");
    mitarea = "0" + $(this).data("tarea");
    kolorea = $(this).data('kolorea');
    inzidentzia = $(this).data("incidencia");
    switch (accion) {
        case "sale":
            funcsalir(miimg, mirel, mitarea, kolorea, inzidentzia);
            break;
        case "entra":
            funcentrar(miimg, mirel, mitarea, kolorea);
            break;
    }
}

function funcsalir(dest, mirel, mitarea, kolorea, inzidentzia) {


    var misrc = "/bundles/registro/img/plano2/" + mirel + "-" + mitarea + "-" + kolorea + "-" + inzidentzia + ".png";
    $(dest).attr("src", misrc);

    // AJAX: actualizamos el estado de ocupación del a habitación
    var url =" {{ path('estadohabitacion')}}";

    $.post(url, { hab: mirel }, function(data) { } );
}

function funcentrar( dest, mirel, mitarea) {
    var misrc = "/bundles/registro/img/plano2/" + mirel + "-gorria.png";
    $(dest).attr("src", misrc);

    // AJAX: actualizamos el estado de ocupación del a habitación
    var url =" {{ path('estadohabitacion')}}";

    $.post(url, { hab: mirel }, function(data) { } );
}

$('#asignartodos').click(function(){
    var usu =  $("select option:selected").text();
    var nusu = $("select option:selected").val();

    if ( nusu==="") {
        alert("Selecciona usuario primero");
        return;
    }

    $('.hidden-usuario').each(function(index) {
         if ( ($(this).val()==="") ||  ( $(this).val() != nusu )) {
            $(this).val(nusu);
         }
    });

    $('.hidden-nombreusuario').each(function(index) {
         if ( ($(this).val()==="") ||  ( $(this).val() != usu )) {
            $(this).val(usu);
         }
    });

    $('.hotel_gela').each(function (index) {
        $(this).prev().text(usu);
    });


});

$('#btn_asignar').click(function() {
    $('#bilatu').submit();
});

$('.hotel_gela').click(function(e) {
    var usu =  $("select option:selected").text();
    var nusu = $("select option:selected").val();
    var miva = $(this).next('input').val();
    console.log("usu: " + usu);
    console.log("nusu: " + nusu);
    console.log("miva: " + miva);
    if ( miva === nusu ) {
        $(this).next('input').val("");
        $(this).next('input').next().val("");
        $(this).prev('span').text("");
    }else if ( (miva != nusu) || ( miva === "" )) {
        $(this).next('input').val(nusu);
        $(this).next('input').next().val(usu);
        $(this).prev('span').text(usu);
    }

});

$('#btnasignartarea').click(function() {
    $('#miq').val($('#fetxa').val());
    $('#frmasignarlimpieza').submit();
});

$('#btnasign').click(function() {
    $('#miq0').val($('#q').val());
    $('#frmasignarlimpieza2').submit();
});