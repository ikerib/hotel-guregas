<?php

namespace Gitek\HotelBundle\Helpers;

class Mapatareas
{
    function __construct($entityManager, $container)
    {
        $this->em = $entityManager;
        $this->container = $container;
    }

    function getMapatareas($q = null, $usu = null)
    {

        $erantzuna = "";
        $cont = -1;
        $hab = "";
        $fece = "";
        $fecs = "";
        $eztakadatuik = 0;

        if (is_null($q)) {
            $q = Date('Y-m-d');
        }
        // print_r("Q DA: " . $q);
        // if ( $q != null ) {
        //     // $client = new \SoapClient("http://www.gestiondatos.com/serviciosweb/wshotelbeasain.asmx?WSDL");
        if (is_null($usu)) {
            $registros = $this->em->getRepository('RegistroBundle:Registro')->registrosporfechaordenadoporhabitacion(Date($q));
        } else {
            $registros = $this->em->getRepository('RegistroBundle:Registro')->registrosporfechaordenadoporhabitacionusuario(Date($q), $usu);
            if (count($registros) == 0) {
                $eztakadatuik = 1; // Si es la pantalla de usuarios y no hay datos, todos ocupados
            }
        }

        if (count($registros) > 0) {
            // print("han");
            foreach ($registros as $r) {
                // ladybug_dump($r);
                $hab = $r->getHabitacion()->getNombre();
                $habid = $r->getHabitacion()->getId();
                $tar = "0" . $r->getTipotarea()->getId();
                // $erantzuna[$habid]["FechaEntrada"]="";
                $erantzuna[$habid]["FechaEntrada"] = date('Y-m-d');
                $erantzuna[$habid]["Tarea"] = $tar;
                $erantzuna[$habid]["Habitacion"] = $hab;
                $erantzuna[$habid]["Comenzado"] = $r->getComenzado();
                $erantzuna[$habid]["Completado"] = $r->getCompletado();
                $ocupado = $this->getocupado($hab, $q);
                $erantzuna[$habid]["Ocupado"] = $ocupado;
                $erantzuna[$habid]["Usuario"] = $r->getUsuario();
                // Si tiene incidencia o no
                $incidencia = $this->getincidenciahabitacionfecha($habid, $q); // Miramos si tiene una incidencia
                $erantzuna[$habid]["Incidencia"] = $incidencia;
            }

        } else {
            // print_r("Fetxarik gabe");
            for ($cont = 1; $cont < 11; $cont++) {
                $erantzuna[$cont]["FechaEntrada"] = date('Y-m-d');
                $erantzuna[$cont]["Tarea"] = "03";
                if ($cont < 10) {
                    $nombrehabitacion = "10" . $cont;
                } else {
                    $nombrehabitacion = "110";
                }
                $hab = $this->em->getRepository('HotelBundle:Habitacion')->findOneByNombre($nombrehabitacion);
                $erantzuna[$cont]["Habitacion"] = $hab;
                $erantzuna[$cont]["Comenzado"] = "";
                $erantzuna[$cont]["Completado"] = "0";
                $erantzuna[$cont]["Ocupado"] = "0";
                $erantzuna[$cont]["Incidencia"] = "0";
                $usu = $this->em->getRepository('UsuarioBundle:Usuario')->findOneById(2);
                $erantzuna[$cont]["Usuario"] = $usu;
            }
        }

        $erantzuna = $this->magia($erantzuna);
        // ladybug_dump($erantzuna);
        return $erantzuna;
        // } //endif
    }

    function magia($erantzuna)
    {
        // funcion para componer el nombre de la imagen
        $img = "";
        $color = "";
        foreach ($erantzuna as $key => $value) {
            if ($value["Ocupado"] == 1) {
                $img = $value['Habitacion'] . "-gorria.png";
            } elseif ($value["Incidencia"] == 1) {
                $img = $value['Habitacion'] . "-" . $value['Tarea'] . "-berdea-1.png";
            } else {
                if ($value['Completado'] == 1) {
                    $img = $value['Habitacion'] . "-" . $value['Tarea'] . "-" . "berdea-0.png";
                } else {
                    if ($value['Comenzado'] == 1) {
                        $color = "oria";
                        $img = $value['Habitacion'] . "-" . $value['Tarea'] . "-" . $color . "-" . $value['Incidencia'] . ".png";
                    } else {
                        $color = "gorria";
                        $img = $value['Habitacion'] . "-" . $value['Tarea'] . "-" . $color . ".png";
                    }

                }
            }
            $erantzuna[$key]['img'] = "bundles/registro/img/plano2/" . $img;
            $erantzuna[$key]['Kolorea'] = $color;
        }

        return $erantzuna;
    }

    function getincidencia($hab)
    {
        $registro = $this->em->getRepository('RegistroBundle:Registro')->estadohabitacion($hab);
        if (is_null($registro)) {
            return 0;
        } else {
            if (!is_null($registro->getDetalles())) {
                if ($registro->getDetalles()->count() > 0) {
                    return 1;
                } else {
                    return 0;
                }
            } else {
                return 0;
            }
        }
    }

    function getcompletado($hab)
    {
        $registro = $this->em->getRepository('RegistroBundle:Registro')->estadohabitacion($hab);
        if (is_null($registro)) {
            return 0;
        } else {
            return $registro->getCompletado();
        }
    }

    function getcomenzado($hab)
    {
        $registro = $this->em->getRepository('RegistroBundle:Registro')->estadohabitacion($hab);
        if (is_null($registro)) {
            return 0;
        } else {
            return $registro->getComenzado();
        }
    }

    function getincidenciahabitacionfecha($hab, $fecha)
    {
        $registro = $this->em->getRepository('RegistroBundle:Registro')->estadohabitacionfecha($hab, $fecha);
        if (is_null($registro)) {
            return 0;
        } elseif ($registro->getCompletado() == 0) {
            return 0;
        } else {
            if (!is_null($registro->getIncidencias())) {
                if ($registro->getIncidencias()->count() > 0) {
                    return 1;
                } else {
                    $tipo = $registro->getTipotarea()->getId();
                    $tipotarea = $this->em->getRepository('HotelBundle:Tipotarea')->findOneById($tipo);

                    if ($tipotarea) {
                        $todaslastareas = $tipotarea->getTareas()->count();
                    } else {
                        $todaslastareas = 0;
                    }

                    $habitacion = $this->em->getRepository("HotelBundle:Habitacion")->findOneById($hab);
                    $hab = $habitacion->getNombre();

                    // $mistareas = $this->em->getRepository('RegistroBundle:Registro')->tareasdelregistroporfechausuario($hab,$fecha);

                    $tareas = $this->em->getRepository('RegistroBundle:Registro')->tareasdelahabitacionportipo($hab, $tipo);

                    $registro = $this->em->getRepository('RegistroBundle:Registro')->tareasdelahabitacion($hab, $fecha);

                    // print_r(" Habitación $hab <br /> ");
                    // print_r(" Todas las tareas: $todaslastareas <br />");
                    $tareasrealizadas = $registro->getDetalles()->count();
                    // print_r(" Tareas realizadas: $tareasrealizadas <br /> ");

                    if ($todaslastareas > $tareasrealizadas) {
                        return 1;
                    } else {
                        return 0;
                    }
                }
            }
        }
    }

    function getocupado($hab, $q)
    {
        $fetxa = Date('Y-m-d');
        if ($q == $fetxa) { // Solo mostrará las habitaciones ocupadas cuando sea el día de hoy
            $habitacion = $this->em->getRepository('HotelBundle:Habitacion')->findOneBy(array('nombre' => $hab));
            if (count($habitacion) > 0) {
                return $habitacion->getOcupado();
            } else {
                return 0;
            }
        } else {
            return 0;
        }
    }

    function getnombre($hab)
    {
        $habitacion = $this->em->getRepository('HotelBundle:Habitacion')->findOneBy(array('id' => $hab));
        return $habitacion->getNombre();
    }

    function quetarea($dias)
    {
        // print_r("Quetarean da: " . $dias . "<br />");
        while ($dias > 3) {
            $dias = floor($dias / 3);
        }

        // print_r(" Dias da : " . $dias . "<br />");
        switch ($dias) {
            case ($dias <= 1):
                return "01";
                break;
            case (($dias > 1) && ($dias <= 2)):
                return "02";
                break;
            case ($dias > 2):
                return "03";
                break;
        }
    }
}
