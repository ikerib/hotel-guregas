<?php

namespace Gitek\HotelBundle\Helpers;

class Mapatareas
{
    function __construct($entityManager, $container) {
        $this->em = $entityManager;
        $this->container = $container;
    }

    function getMapatareas($q=null, $usu=null) {

        $erantzuna="";
        $cont=-1;
        $hab = "";
        $fece= "";
        $fecs= "";
        $eztakadatuik = 0;

        if (is_null($q)) {
            $q = Date('Y-m-d');
        }

        if (is_null($usu)) {
            // print_r($usu);
            $registros  = $this->em->getRepository('RegistroBundle:Registro')->registrosporfechaordenadoporhabitacion(Date($q));
        } else {
            // print_r("No usu");
            // print_r($usu);
            $registros  = $this->em->getRepository('RegistroBundle:Registro')->registrosporfechaordenadoporhabitacionusuario(Date($q),$usu);

            if ( count($registros) == 0 ) {
                $eztakadatuik = 1; // Si es la pantalla de usuarios y no hay datos, todos ocupados
            }
        }

        if ( count($registros) > 0) {
            // print_r("Hay registros");
            foreach ( $registros as $r ) {
                $hab = $r->getHabitacion()->getNombre();
                $habid = $r->getHabitacion()->getId();
                $tar = "0" . $r->getTipotarea()->getId();
                $erantzuna[$habid]["FechaEntrada"]="";
                $erantzuna[$habid]["FechaEntrada"]="";
                $erantzuna[$habid]["Tarea"]=$tar;
                $erantzuna[$habid]["Habitacion"]=$hab;
                $erantzuna[$habid]["Comenzado"]=$r->getComenzado();
                $erantzuna[$habid]["Completado"]=$r->getCompletado();
                $ocupado = $this->getocupado($habid,$q);
                // print_r(" okupatua </br> ");
                // ladybug_dump( $ocupado );
                $erantzuna[$habid]["Ocupado"]=$ocupado;
                $erantzuna[$habid]["Usuario"]=$r->getUsuario();
                // Si tiene incidencia o no

                // $incidencia = $this->getincidenciahabitacionfecha($habid, $q); // Miramos si tiene una incidencia
                // print_r("Inzidentzia da: $incidencia ");
                // print_r("Inzidentzia zenb: " . count($r->getIncidencias()));
                $erantzuna[$habid]["Incidencia"]=count($r->getIncidencias());
            }

            // Añadimos los que faltan
            for ( $cont=1; $cont<11; $cont++) {
                $aurkitua=0;
                foreach ($registros as $r) {
                    if ( $r->getHabitacion()->getId() == $cont ) {
                        $aurkitua=1;
                    }
                }
                if ( $aurkitua ==0 ) {
                    // ladybug_dump( $registros );
                    if (!$registros) {
                        $tar ="01";
                    } else {
                        $tar = "0" . $registros[0]->getTipotarea()->getId();
                    }

                    if ($cont<10) {
                        $nombrehabitacion="10".$cont;
                    } else {
                        $nombrehabitacion="110";
                    }
                    $hab = $this->em->getRepository('HotelBundle:Habitacion')->findOneByNombre($nombrehabitacion);

                    $erantzuna[$cont]["FechaEntrada"]="";
                    $erantzuna[$cont]["FechaEntrada"]="";
                    $erantzuna[$cont]["Tarea"]=$tar;
                    $erantzuna[$cont]["Habitacion"]=$hab;
                    $erantzuna[$cont]["Comenzado"]="0";
                    $erantzuna[$cont]["Completado"]="0";
                    $erantzuna[$cont]["Ocupado"]="1";
                    $erantzuna[$cont]["Incidencia"]="0";
                }
            }

        } else {
            // print_r("WebService");
            $client = new \SoapClient("http://www.gestiondatos.com/serviciosweb/wshotelbeasain.asmx?WSDL");

            $response = $client->EstadoHabitacionesFechas();

            $mijson=json_decode($response->EstadoHabitacionesFechasResult);

            if ( count($mijson) > 0 ) {
                foreach ($mijson as $j) {
                    $cont+=1;
                    foreach ($j as $key => $value) {
                        switch ($key) {
                            case "IdHabitacion":
                                $hab = $value;
                                break;
                            case "FechaEntrada":
                                if ($fece=="") {
                                    $fece = $value;
                                } else {
                                    if ( $fece >$value ){
                                        $fece = $value;
                                    }
                                }
                                break;
                            case "FechaSalida":
                                if ($fecs=="") {
                                    $fecs = $value;
                                } else {
                                    if ( $fecs >$value ){
                                        $fecs = $value;
                                    }
                                }
                                break;
                        }
                    }
                    $erantzuna[$hab]["FechaEntrada"]=$fece;
                    $erantzuna[$hab]["FechaSalida"]=$fecs;

                    if ($j->FechaEntrada) {
                        $date1 = \DateTime::createFromFormat('d/m/Y',$j->FechaEntrada);
                        $date2 = new \DateTime(date($q));
                        $interval = $date1->diff($date2);

                        $mitarea = $this->quetarea($interval->days);

                    } else {
                        $mitarea = $this->quetarea(1);
                    }

                    $erantzuna[$hab]["Tarea"]=$mitarea;
                    $erantzuna[$hab]["Habitacion"]=$hab;

                    // Si la tarea ha sido comenzada o no
                    $comenzado = $this->getcomenzado($hab);
                    $erantzuna[$hab]["Comenzado"]=$comenzado;

                    // Si tarea completada o no
                    $completado = $this->getcompletado($hab);
                    $erantzuna[$hab]["Completado"]=$completado;

                    // Si habitación ocupado o no
                    if ( $eztakadatuik ==0 ) {
                        // print_r(" Okupatua ON <br />");
                        $ocupado = $this->getocupado($hab,$q);
                        // ladybug_dump( $ocupado );
                        $erantzuna[$hab]["Ocupado"]=$ocupado;
                    } else {
                        // print_r(" Okupatua OFF <br />");
                        $erantzuna[$hab]["Ocupado"]=1;
                    }


                    // Nombre de la habitación
                    $nombre = $this->getnombre($hab);
                    $erantzuna[$hab]["Habitacion"]=$nombre;

                    // Si tiene incidencia o no
                    $incidencia = $this->getincidencia($hab);
                    $erantzuna[$hab]["Incidencia"]=$incidencia;

                    // Establecemos el usuario por decto

                    $usu = $this->container->getParameter('gitek.usuario_por_defecto');
                    $erantzuna[$hab]["Usuario"] = $usu;


                } // end foreach

                for ( $cont=1; $cont<11; $cont++) {
                    $aurkitua=0;
                    foreach ($erantzuna as $key => $value) {
                      // ladybug_dump( $key );
                        if ( $key== $cont ) {
                            $aurkitua=1;
                        }
                    }
                    if ( $aurkitua ==0 ) {
                        // ladybug_dump( $registros );
                        if (!$registros) {
                            $tar ="01";
                        } else {
                            $tar = "0" . $registros[0]->getTipotarea()->getId();
                        }

                        if ($cont<10) {
                            $nombrehabitacion="10".$cont;
                        } else {
                            $nombrehabitacion="110";
                        }
                        $hab = $this->em->getRepository('HotelBundle:Habitacion')->findOneByNombre($nombrehabitacion);

                        $erantzuna[$cont]["FechaEntrada"]="";
                        $erantzuna[$cont]["FechaEntrada"]="";
                        $erantzuna[$cont]["Tarea"]=$tar;
                        $erantzuna[$cont]["Habitacion"]=$hab->getNombre();
                        $erantzuna[$cont]["Comenzado"]="0";
                        $erantzuna[$cont]["Completado"]="0";
                        // $erantzuna[$cont]["Ocupado"]=$hab->getOcupado();
                        // Si habitación ocupado o no
                        if ( $eztakadatuik ==0 ) {
                            // print_r(" Okupatua ON <br />");
                            $ocupado = $this->getocupado($hab,$q);
                            // ladybug_dump( $ocupado );
                            $erantzuna[$cont]["Ocupado"]=$ocupado;
                        } else {
                            // print_r(" Okupatua OFF <br />");
                            $erantzuna[$cont]["Ocupado"]=1;
                        }
                        $erantzuna[$cont]["Incidencia"]="0";
                        // Establecemos el usuario por decto
                        $usu = $this->container->getParameter('gitek.usuario_por_defecto');
                        $erantzuna[$cont]["Usuario"] = $usu;
                        // print_r($usu);
                        // print_r("**************** <br/>");
                    }
                }
            } else {
                $erantzuna= $this->mapahutsa();
            } // if ( count($mijson) > 0 ) {
        }
        $erantzuna = $this->magia($erantzuna);
        return $erantzuna;
    }


    function mapahutsa() {
        // Añadimos los que faltan
        for ( $cont=1; $cont<11; $cont++) {
            $tar ="01";

            if ($cont<10) {
                $nombrehabitacion="10".$cont;
            } else {
                $nombrehabitacion="110";
            }
            $hab = $this->em->getRepository('HotelBundle:Habitacion')->findOneByNombre($nombrehabitacion);
            $erantzuna[$cont]["FechaEntrada"]="";
            $erantzuna[$cont]["FechaEntrada"]="";
            $erantzuna[$cont]["Tarea"]=$tar;
            $erantzuna[$cont]["Habitacion"]=$hab;
            $erantzuna[$cont]["Comenzado"]="0";
            $erantzuna[$cont]["Completado"]="0";
            $erantzuna[$cont]["Ocupado"]="1";
            $erantzuna[$cont]["Incidencia"]="0";
        }
        return $erantzuna;
    }

    function magia($erantzuna) {
        // funcion para componer el nombre de la imagen
        $img = "";
        $color="";

        foreach ($erantzuna as $key => $value) {
            // print_r("*************************");
            // print_r("Habitacion: " . $value['Habitacion'] ." <br/>");
            // print_r("Ocupado: ". $value['Ocupado'] . "<br/>");
            // print_r("Comenzado: " . $value['Comenzado'] . "<br/>");
            // print_r("Completado: ". $value['Completado'] . "<br/>");
            // print_r("Incidencia: " . $value['Incidencia'] . "<br/>");

            if ( $value["Ocupado"] == 1 ) {
                // print_r(" 1 <br />");
                $img = $value['Habitacion'] . "-gorria.png";
            } elseif ( $value["Incidencia"] > 0) {
                // print_r(" INCIDENCIAA!!!! <br />");
                if (( $value["Comenzado"] == 1) && ( $value["Completado"]==0)){
                    $img = $value['Habitacion'] . "-" . $value['Tarea'] . "-oria-1.png";
                } elseif (( $value["Comenzado"] == 1) && ( $value["Completado"]==1)){
                    $img = $value['Habitacion'] . "-" . $value['Tarea'] . "-berdea-1.png";
                } else {
                    $img = $value['Habitacion'] . "-" . $value['Tarea'] . "-berdea-1.png";
                }
            } else {
                // print_r(" 111  <br />");
                if ( $value['Completado']==1 ) {
                    // print_r(" 1111  <br />");
                    $img = $value['Habitacion'] . "-" . $value['Tarea'] . "-" . "berdea-0.png";
                } else {
                    // print_r(" 11111  <br />");
                    if ( ($value['Comenzado']==1) && ( $value['Completado']==0 ) ) {
                        // print_r(" ORIA!!  <br />");
                        $color="oria";
                        $img = $value['Habitacion'] . "-" . $value['Tarea'] . "-" .$color."-".$value['Incidencia'].".png";
                    } else {
                        // print_r(" hemen!! ");
                        // print_r( $value['Completado']);
                        if ( $value['Completado']=="1") {
                            $color="berdea";
                            $img = $value['Habitacion'] . "-" . $value['Tarea'] . "-" .$color."-".$value['Incidencia'].".png";
                        } else {
                            $color="gorria";
                            $img = $value['Habitacion'] . "-" . $value['Tarea'] . "-" .$color.".png";
                        }

                    }

                }
            }
            $erantzuna[$key]['img']="bundles/registro/img/plano2/".$img;
            $erantzuna[$key]['Kolorea']=$color;
        }
        return $erantzuna;
    }

    function getincidencia($hab) {
        $registro = $this->em->getRepository('RegistroBundle:Registro')->estadohabitacion($hab);
        if ( is_null($registro)) {
            return 0;
        } else {
            if (!is_null($registro->getDetalles())) {
                if ( $registro->getDetalles()->count() > 0) {
                   return 1;
                } else {
                    return 0;
                }
            } else {
                return 0;
            }
        }
    }

    function getcompletado($hab) {
        $registro = $this->em->getRepository('RegistroBundle:Registro')->estadohabitacion($hab);
        if ( is_null($registro)) {
            return 0;
        } else {
            return $registro->getCompletado();
        }
    }

    function getcomenzado($hab) {
        $registro = $this->em->getRepository('RegistroBundle:Registro')->estadohabitacion($hab);
        if ( is_null($registro)) {
            return 0;
        } else {
            return $registro->getComenzado();
        }
    }

    function getincidenciahabitacionfecha($hab, $fecha) {
        $registro = $this->em->getRepository('RegistroBundle:Registro')->estadohabitacionfecha($hab, $fecha);
        if ( is_null($registro)) {
            return 0;
        } elseif ( $registro->getCompletado()==0) {
            return 0;
        } else {
            if ( !is_null($registro->getIncidencias())) {
                if ($registro->getIncidencias()->count() > 0)  {
                    return 1;
                } else {
                    $tipo = $registro->getTipotarea()->getId();
                    $tipotarea = $this->em->getRepository('HotelBundle:Tipotarea')->findOneById($tipo);

                    if ( $tipotarea ) {
                        $todaslastareas = $tipotarea->getTareas()->count();
                    } else {
                        $todaslastareas = 0;
                    }

                    $habitacion = $this->em->getRepository("HotelBundle:Habitacion")->findOneById($hab);
                    $hab = $habitacion->getNombre();

                    // $mistareas = $this->em->getRepository('RegistroBundle:Registro')->tareasdelregistroporfechausuario($hab,$fecha);

                    $tareas = $this->em->getRepository('RegistroBundle:Registro')->tareasdelahabitacionportipo($hab,$tipo);

                    $registro       = $this->em->getRepository('RegistroBundle:Registro')->tareasdelahabitacion($hab,$fecha);

                    // print_r(" Habitación $hab <br /> ");
                    // print_r(" Todas las tareas: $todaslastareas <br />");
                    $tareasrealizadas = $registro->getDetalles()->count();
                    // print_r(" Tareas realizadas: $tareasrealizadas <br /> ");

                    if ( $todaslastareas > $tareasrealizadas ) {
                        return 1;
                    } else {
                        return 0;
                    }
                }
            }
        }
    }

    function getocupado($hab, $q) {
        $fetxa = Date('Y-m-d');
        if ( $q == $fetxa ) { // Solo mostrará las habitaciones ocupadas cuando sea el día de hoy
            // print_r(" sartu da <br />");
            // print_r(" Hab da $hab da <br />");
            // $habitacion = $this->em->getRepository('HotelBundle:Habitacion')->findOneBy(array('nombre' => $hab));
            $habitacion = $this->em->getRepository('HotelBundle:Habitacion')->findOneById($hab);
            // ladybug_dump( $habitacion );
            if ( count($habitacion) > 0) {
                return $habitacion->getOcupado();
            } else {
                return 0;
            }
        } else {
            return 0;
        }
    }

    function getnombre($hab) {
        $habitacion = $this->em->getRepository('HotelBundle:Habitacion')->findOneBy(array('id' => $hab));
        return $habitacion->getNombre();
    }

    function quetarea($dias) {
        // print_r("Quetarean da: " . $dias . "<br />");
        while ( $dias > 3 ) {
            $dias = floor($dias / 3);
        }

        // print_r(" Dias da : " . $dias . "<br />");
        switch ($dias){
            case ($dias <= 1):
                return "01";
                break;
            case (($dias > 1)  && ( $dias <= 2)):
                return "02";
                break;
            case ( $dias > 2):
                return "03";
                break;
        }
    }
}
