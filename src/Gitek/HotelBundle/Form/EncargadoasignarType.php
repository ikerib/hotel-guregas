<?php

namespace Gitek\HotelBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Doctrine\ORM\EntityRepository;

class EncargadoasignarType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('usuario',
                'entity',
                array(
                    'class' => 'Gitek\\UsuarioBundle\\Entity\\Usuario',
                    'empty_value' => 'Selecciona una usuario',
                    'query_builder' => function(EntityRepository $repositorio)
                    {
                        return $repositorio->createQueryBuilder('c')
                                ->innerJoin('c.tipousuario','t')
                                ->where("t.id = :tipoa")
                                ->and("c.enabled=1")
                                ->orderBy('c.apellidos,c.nombre', 'ASC')
                                ->setParameter('tipoa', 3);
                    },
                )
            )
            ->add('fecha','date', array(
                'attr' => array('class' => 'somedatefield'),
                'widget' => 'single_text',
                'format' => 'yy-mm-dd',
                    )
                )
            ->add('habitacion')
            ->add('tipotarea')
        ;
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
                'data_class' => 'Gitek\RegistroBundle\Entity\Registro',
            ));
    }

    public function getName()
    {
        return 'encargado_asignartype';
    }
}