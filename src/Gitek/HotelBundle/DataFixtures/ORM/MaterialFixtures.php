<?php

namespace Gitek\HotelBundle\DataFixtures\ORM;

use Doctrine\Common\Persistence\ObjectManager; 
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Gitek\HotelBundle\Entity\Material;

class MaterialFixtures extends AbstractFixture implements OrderedFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        for($i=0;$i<11;$i++){
            $material = new Material();
            $material->setNombre("material-".$i);
            $manager->persist($material);   

            $this->addReference('material-'.$i, $material);
        }
        $manager->flush();
    }

    public function getOrder()
    {
        return 2; 
    }
}