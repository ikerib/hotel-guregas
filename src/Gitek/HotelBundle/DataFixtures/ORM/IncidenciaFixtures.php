<?php

namespace Gitek\HotelBundle\DataFixtures\ORM;

use Doctrine\Common\Persistence\ObjectManager; 
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Gitek\HotelBundle\Entity\Incidencia;

class IncidenciaFixtures extends AbstractFixture implements OrderedFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        for($i=0;$i<11;$i++){
            $incidencia = new Incidencia();
            $incidencia->setNombre("incidencia-".$i);
            $manager->persist($incidencia);   
        }
        $manager->flush();
    }

    public function getOrder()
    {
        return 3; 
    }
}