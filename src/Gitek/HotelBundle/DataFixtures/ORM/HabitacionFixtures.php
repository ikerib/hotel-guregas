<?php

namespace Gitek\HotelBundle\DataFixtures\ORM;

use Doctrine\Common\Persistence\ObjectManager; 
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Gitek\HotelBundle\Entity\Habitacion;

class HabitacionFixtures extends AbstractFixture implements OrderedFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        for($i=101;$i<111;$i++){
            $habitacion = new Habitacion();
            $habitacion->setNombre($i);
            $habitacion->setOcupado(0);
            $manager->persist($habitacion);   
        }
        $manager->flush();
    }

    public function getOrder()
    {
        return 1; 
    }
}