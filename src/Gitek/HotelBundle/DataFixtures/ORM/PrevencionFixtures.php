<?php

namespace Gitek\HotelBundle\DataFixtures\ORM;

use Doctrine\Common\Persistence\ObjectManager; 
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Gitek\HotelBundle\Entity\Prevencion;

class PrevencionFixtures extends AbstractFixture implements OrderedFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        for($i=0;$i<11;$i++){
            $prevencion = new Prevencion();
            $prevencion->setNombre("prevencion-".$i);
            $manager->persist($prevencion);   

            $this->addReference('prevencion-'.$i, $prevencion);
        }
        $manager->flush();
    }

    public function getOrder()
    {
        return 4; 
    }
}