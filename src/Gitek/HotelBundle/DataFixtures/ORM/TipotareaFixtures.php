<?php

namespace Gitek\HotelBundle\DataFixtures\ORM;

use Doctrine\Common\Persistence\ObjectManager; 
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface; 
use Gitek\HotelBundle\Entity\Tipotarea;

class TipotareaFixtures extends AbstractFixture implements OrderedFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $tipotarea = new Tipotarea();
        $tipotarea->setNombre("1º Día");
        $manager->persist($tipotarea);   

        $tipotarea2 = new Tipotarea();
        $tipotarea2->setNombre("Repaso");
        $manager->persist($tipotarea2);   
        
        $tipotarea3 = new Tipotarea();
        $tipotarea3->setNombre("Completo");
        $manager->persist($tipotarea3);   

        $manager->flush();

        $this->addReference('tipotarea', $tipotarea);
        $this->addReference('tipotarea2', $tipotarea2);
        $this->addReference('tipotarea3', $tipotarea3);
    }

    public function getOrder()
    {
        return 5; 
    }
}