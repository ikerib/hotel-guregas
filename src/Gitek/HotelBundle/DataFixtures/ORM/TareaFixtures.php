<?php

namespace Gitek\HotelBundle\DataFixtures\ORM;

use Doctrine\Common\Persistence\ObjectManager; 
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface; 
use Gitek\HotelBundle\Entity\Tarea;
use Gitek\HotelBundle\Entity\Tipotarea;
use Gitek\HotelBundle\Entity\Material;

class TareaFixtures extends AbstractFixture implements OrderedFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $tarea = new Tarea();
        $tarea->setVideo('XadfwGssSaU');
        $tarea->setNombre("Fregar el suelo");
        $tarea->setTipotarea($manager->merge($this->getReference('tipotarea')));
        $tarea->addMaterial($manager->merge($this->getReference('material-1')));
        $tarea->addMaterial($manager->merge($this->getReference('material-3')));
        $tarea->addMaterial($manager->merge($this->getReference('material-4')));
        $tarea->addPrevencion($manager->merge($this->getReference('prevencion-1')));
        $tarea->addPrevencion($manager->merge($this->getReference('prevencion-3')));
        $tarea->addPrevencion($manager->merge($this->getReference('prevencion-5')));
        $tarea->addPrevencion($manager->merge($this->getReference('prevencion-6')));
        $tarea->addPrevencion($manager->merge($this->getReference('prevencion-7')));
        $tarea->addPrevencion($manager->merge($this->getReference('prevencion-8')));        
        $manager->persist($tarea);   

        $tarea = new Tarea();
        $tarea->setVideo('XadfwGssSaU');
        $tarea->setNombre("Cambiar sabanas");
        $tarea->setTipotarea($manager->merge($this->getReference('tipotarea')));
        $tarea->addMaterial($manager->merge($this->getReference('material-2')));
        $tarea->addMaterial($manager->merge($this->getReference('material-5')));
        $tarea->addMaterial($manager->merge($this->getReference('material-6')));
        $tarea->addPrevencion($manager->merge($this->getReference('prevencion-5')));
        $tarea->addPrevencion($manager->merge($this->getReference('prevencion-6')));
        $tarea->addPrevencion($manager->merge($this->getReference('prevencion-7')));
        $tarea->addPrevencion($manager->merge($this->getReference('prevencion-8'))); 
        $manager->persist($tarea); 

        $tarea = new Tarea();
        $tarea->setVideo('XadfwGssSaU');
        $tarea->setNombre("Quitar el polvo");
        $tarea->setTipotarea($manager->merge($this->getReference('tipotarea')));
        $tarea->addMaterial($manager->merge($this->getReference('material-1')));
        $tarea->addMaterial($manager->merge($this->getReference('material-3')));
        $tarea->addMaterial($manager->merge($this->getReference('material-5')));
        $tarea->addMaterial($manager->merge($this->getReference('material-6')));
        $tarea->addMaterial($manager->merge($this->getReference('material-7')));
        $tarea->addMaterial($manager->merge($this->getReference('material-8')));
        $tarea->addPrevencion($manager->merge($this->getReference('prevencion-5')));
        $tarea->addPrevencion($manager->merge($this->getReference('prevencion-6')));
        $tarea->addPrevencion($manager->merge($this->getReference('prevencion-7')));
        $tarea->addPrevencion($manager->merge($this->getReference('prevencion-8')));         
        $manager->persist($tarea); 
        
        $tarea = new Tarea();
        $tarea->setVideo('XadfwGssSaU');
        $tarea->setNombre("Hacer baño");
        $tarea->setTipotarea($manager->merge($this->getReference('tipotarea')));
        $tarea->addMaterial($manager->merge($this->getReference('material-1')));
        $tarea->addMaterial($manager->merge($this->getReference('material-2')));
        $tarea->addMaterial($manager->merge($this->getReference('material-3')));
        $tarea->addMaterial($manager->merge($this->getReference('material-9')));
        $tarea->addMaterial($manager->merge($this->getReference('material-7')));
        $tarea->addMaterial($manager->merge($this->getReference('material-8')));
        $tarea->addPrevencion($manager->merge($this->getReference('prevencion-1')));
        $tarea->addPrevencion($manager->merge($this->getReference('prevencion-3')));
        $manager->persist($tarea);         
        
        $tarea = new Tarea();
        $tarea->setVideo('XadfwGssSaU');
        $tarea->setNombre("Fregar el suelo");
        $tarea->setTipotarea($manager->merge($this->getReference('tipotarea2')));
        $tarea->addMaterial($manager->merge($this->getReference('material-7')));
        $tarea->addMaterial($manager->merge($this->getReference('material-8')));
        $tarea->addPrevencion($manager->merge($this->getReference('prevencion-1')));
        $tarea->addPrevencion($manager->merge($this->getReference('prevencion-3')));
        $manager->persist($tarea);         

        $tarea = new Tarea();
        $tarea->setVideo('XadfwGssSaU');
        $tarea->setNombre("Quitar polvo");
        $tarea->setTipotarea($manager->merge($this->getReference('tipotarea')));
        $tarea->addMaterial($manager->merge($this->getReference('material-1')));
        $tarea->addMaterial($manager->merge($this->getReference('material-2')));
        $tarea->addMaterial($manager->merge($this->getReference('material-3')));
        $tarea->addMaterial($manager->merge($this->getReference('material-4')));
        $tarea->addMaterial($manager->merge($this->getReference('material-5')));
        $tarea->addMaterial($manager->merge($this->getReference('material-6')));
        $tarea->addPrevencion($manager->merge($this->getReference('prevencion-5')));
        $tarea->addPrevencion($manager->merge($this->getReference('prevencion-6')));
        $tarea->addPrevencion($manager->merge($this->getReference('prevencion-7')));
        $tarea->addPrevencion($manager->merge($this->getReference('prevencion-8'))); 
        $manager->persist($tarea); 

        $tarea = new Tarea();
        $tarea->setVideo('XadfwGssSaU');
        $tarea->setNombre("Fregar el suelo");
        $tarea->setTipotarea($manager->merge($this->getReference('tipotarea3')));
        $tarea->addMaterial($manager->merge($this->getReference('material-6')));
        $tarea->addMaterial($manager->merge($this->getReference('material-7')));
        $tarea->addMaterial($manager->merge($this->getReference('material-8')));
        $tarea->addMaterial($manager->merge($this->getReference('material-9')));
        $tarea->addMaterial($manager->merge($this->getReference('material-1')));
        $tarea->addPrevencion($manager->merge($this->getReference('prevencion-5')));
        $tarea->addPrevencion($manager->merge($this->getReference('prevencion-6')));
        $tarea->addPrevencion($manager->merge($this->getReference('prevencion-7')));
        $tarea->addPrevencion($manager->merge($this->getReference('prevencion-8')));         
        $manager->persist($tarea);         

        $manager->flush();


    }

    public function getOrder()
    {
        return 6; 
    }
}