<?php

namespace Gitek\HotelBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Gitek\HotelBundle\Entity\Habitacion
 *
 * @ORM\Entity
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Gitek\HotelBundle\Entity\HabitacionRepository")
 */
class Habitacion
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string $nombre
     *
     * @ORM\Column(name="nombre", type="string", length=100)
     */
    private $nombre;

    /**
     * @var smallint $ocupado
     *
     * @ORM\Column(name="ocupado", type="smallint", nullable=true)
     */
    private $ocupado=0;

    /**
     * @var datetime $fecha_entrada
     *
     * @ORM\Column(name="fecha_entrada", type="datetime", nullable=true)
     */
    private $fecha_entrada;

    /**
     * @var datetime $fecha_salida
     *
     * @ORM\Column(name="fecha_salida", type="datetime", nullable=true)
     */
    private $fecha_salida;

    /**
     * @var datetime $created_at
     *
     * @ORM\Column(name="created_at", type="datetime")
     */
    private $created_at;

    /**
     * @var datetime $updated_at
     *
     * @ORM\Column(name="updated_at", type="datetime")
     */
    private $updated_at;

    public function __construct()
    {
        $this->created_at = new \DateTime();
        $this->updated_at = new \DateTime();
    }
    
    public function __toString()
    {
        return $this->getNombre();
    }


  

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nombre
     *
     * @param string $nombre
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;
    }

    /**
     * Get nombre
     *
     * @return string 
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * Set ocupado
     *
     * @param smallint $ocupado
     */
    public function setOcupado($ocupado)
    {
        $this->ocupado = $ocupado;
    }

    /**
     * Get ocupado
     *
     * @return smallint 
     */
    public function getOcupado()
    {
        return $this->ocupado;
    }

    /**
     * Set fecha_entrada
     *
     * @param datetime $fechaEntrada
     */
    public function setFechaEntrada($fechaEntrada)
    {
        $this->fecha_entrada = $fechaEntrada;
    }

    /**
     * Get fecha_entrada
     *
     * @return datetime 
     */
    public function getFechaEntrada()
    {
        return $this->fecha_entrada;
    }

    /**
     * Set fecha_salida
     *
     * @param datetime $fechaSalida
     */
    public function setFechaSalida($fechaSalida)
    {
        $this->fecha_salida = $fechaSalida;
    }

    /**
     * Get fecha_salida
     *
     * @return datetime 
     */
    public function getFechaSalida()
    {
        return $this->fecha_salida;
    }

    /**
     * Set created_at
     *
     * @param datetime $createdAt
     */
    public function setCreatedAt($createdAt)
    {
        $this->created_at = $createdAt;
    }

    /**
     * Get created_at
     *
     * @return datetime 
     */
    public function getCreatedAt()
    {
        return $this->created_at;
    }

    /**
     * Set updated_at
     *
     * @param datetime $updatedAt
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updated_at = $updatedAt;
    }

    /**
     * Get updated_at
     *
     * @return datetime 
     */
    public function getUpdatedAt()
    {
        return $this->updated_at;
    }
}