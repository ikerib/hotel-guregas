<?php

namespace Gitek\HotelBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\OrderBy;

/**
 * Gitek\HotelBundle\Entity\Tipotarea
 *
 * @ORM\Entity
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Gitek\HotelBundle\Entity\TipotareaRepository")
 */
class Tipotarea
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string $nombre
     *
     * @ORM\Column(name="nombre", type="string", length=100)
     */
    private $nombre;

    /**
     * @var datetime $created_at
     *
     * @ORM\Column(name="created_at", type="datetime")
     */
    private $created_at;

    /**
     * @var datetime $updated_at
     *
     * @ORM\Column(name="updated_at", type="datetime")
     */
    private $updated_at;

    /**
     * @ORM\ManyToMany(targetEntity="Tarea", inversedBy="tipotareas")
     * @ORM\JoinTable(name="tipotarea_tarea",
     *      joinColumns={@ORM\JoinColumn(name="tipotarea_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="tarea_id", referencedColumnName="id")}
     * )
     * @OrderBy({"orden" = "ASC"})
     */
    private $tareas;

    public function __construct()
    {
        $this->created_at = new \DateTime();
        $this->updated_at = new \DateTime();
    }
    
    public function __toString()
    {
        return $this->getNombre();
    }
  

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nombre
     *
     * @param string $nombre
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;
    }

    /**
     * Get nombre
     *
     * @return string 
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * Set created_at
     *
     * @param datetime $createdAt
     */
    public function setCreatedAt($createdAt)
    {
        $this->created_at = $createdAt;
    }

    /**
     * Get created_at
     *
     * @return datetime 
     */
    public function getCreatedAt()
    {
        return $this->created_at;
    }

    /**
     * Set updated_at
     *
     * @param datetime $updatedAt
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updated_at = $updatedAt;
    }

    /**
     * Get updated_at
     *
     * @return datetime 
     */
    public function getUpdatedAt()
    {
        return $this->updated_at;
    }

    /**
     * Add tareas
     *
     * @param Gitek\HotelBundle\Entity\Tarea $tareas
     */
    public function addTarea(\Gitek\HotelBundle\Entity\Tarea $tareas)
    {
        $this->tareas[] = $tareas;
    }

    /**
     * Get tareas
     *
     * @return Doctrine\Common\Collections\Collection 
     */
    public function getTareas()
    {
        return $this->tareas;
    }

    /**
     * Add tareas
     *
     * @param Gitek\HotelBundle\Entity\Tipotarea $tareas
     */
    public function addTipotarea(\Gitek\HotelBundle\Entity\Tipotarea $tareas)
    {
        $this->tareas[] = $tareas;
    }

    /**
     * Remove tareas
     *
     * @param Gitek\HotelBundle\Entity\Tarea $tareas
     */
    public function removeTarea(\Gitek\HotelBundle\Entity\Tarea $tareas)
    {
        $this->tareas->removeElement($tareas);
    }
}