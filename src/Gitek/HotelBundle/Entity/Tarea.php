<?php

namespace Gitek\HotelBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Vich\UploaderBundle\Mapping\Annotation as Vich;

/**
 * Gitek\HotelBundle\Entity\Tarea
 *
 * @ORM\Entity
 * @ORM\Table()
 * @ORM\HasLifecycleCallbacks
 * @Vich\Uploadable
 * @ORM\Entity(repositoryClass="Gitek\HotelBundle\Entity\TareaRepository")
 */
class Tarea
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string $nombre
     *
     * @ORM\Column(name="nombre", type="string", length=100)
     */
    private $nombre;

    /**
     * @Assert\File(
     *     maxSize="1M",
     *     mimeTypes={"image/png", "image/jpeg", "image/pjpeg"}
     * )
     * @Vich\UploadableField(mapping="uploads", fileNameProperty="imagen", nullable=true)
     *
     * @var File $image
     */
    private $image;

    /**
     * @var string $foto
     *
     * @ORM\Column(name="imagen", type="string", length=255, nullable=true)
     */
    private $imagen;

    /**
     * @var integer order
     *
     * @ORM\Column(name="orden", type="integer")
     */
    private $orden;

    /**
     * @var string $video
     *
     * @ORM\Column(name="video", type="string", length=100)
     */
    private $video;

    /**
     * @var datetime $created_at
     *
     * @ORM\Column(name="created_at", type="datetime")
     */
    private $created_at;

    /**
     * @var datetime $updated_at
     *
     * @ORM\Column(name="updated_at", type="datetime")
     */
    private $updated_at;

    /**
     * @ORM\ManyToMany(targetEntity="Tipotarea", mappedBy="tareas")
     */
    protected $tipotareas;

    /**
     * @ORM\ManyToMany(targetEntity="Categoria", mappedBy="tareas")
     */
    protected $categorias;

    /**
     * @ORM\ManyToMany(targetEntity="Material", inversedBy="tareas")
     * @ORM\JoinTable(name="material_tarea",
     *      joinColumns={@ORM\JoinColumn(name="tarea_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="material_id", referencedColumnName="id")}
     * )
     */
    protected $materiales;

    /**
     * @ORM\ManyToMany(targetEntity="Prevencion", inversedBy="tareas")
     * @ORM\JoinTable(name="prevencion_tarea",
     *      joinColumns={@ORM\JoinColumn(name="tarea_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="prevencion_id", referencedColumnName="id")}
     * )
     */
    protected $prevenciones;


//    /**
//     * @ORM\OneToMany(targetEntity="Detcurso", mappedBy="curso", cascade={"remove"})
//     * @ORM\OrderBy({"orden" = "ASC"})
//     */
//    private $detcursos;

    /** @ORM\OneToMany(targetEntity="Gitek\RegistroBundle\Entity\Registrodet", mappedBy="tarea") */
    protected $detalles;

    public function setImage($image) {
        $this -> image = $image;
        if ($image instanceof UploadedFile) {
            $this->setUpdatedAt(new \DateTime());
        }
    }

    public function getImage() {
        return $this->image;
    }

    public function __construct()
    {
        $this->materiales = new \Doctrine\Common\Collections\ArrayCollection();
        $this->prevenciones = new \Doctrine\Common\Collections\ArrayCollection();
        $this->tipotareas = new \Doctrine\Common\Collections\ArrayCollection();
        $this->categorias = new \Doctrine\Common\Collections\ArrayCollection();
        $this->created_at = new \DateTime();
        $this->updated_at = new \DateTime();
    }

    public function __toString()
    {
        return $this->getNombre();
    }



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nombre
     *
     * @param string $nombre
     * @return Tarea
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;
    
        return $this;
    }

    /**
     * Get nombre
     *
     * @return string 
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * Set imagen
     *
     * @param string $imagen
     * @return Tarea
     */
    public function setImagen($imagen)
    {
        $this->imagen = $imagen;
    
        return $this;
    }

    /**
     * Get imagen
     *
     * @return string 
     */
    public function getImagen()
    {
        return $this->imagen;
    }

    /**
     * Set video
     *
     * @param string $video
     * @return Tarea
     */
    public function setVideo($video)
    {
        $this->video = $video;
    
        return $this;
    }

    /**
     * Get video
     *
     * @return string 
     */
    public function getVideo()
    {
        return $this->video;
    }

    /**
     * Set created_at
     *
     * @param \DateTime $createdAt
     * @return Tarea
     */
    public function setCreatedAt($createdAt)
    {
        $this->created_at = $createdAt;
    
        return $this;
    }

    /**
     * Get created_at
     *
     * @return \DateTime 
     */
    public function getCreatedAt()
    {
        return $this->created_at;
    }

    /**
     * Set updated_at
     *
     * @param \DateTime $updatedAt
     * @return Tarea
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updated_at = $updatedAt;
    
        return $this;
    }

    /**
     * Get updated_at
     *
     * @return \DateTime 
     */
    public function getUpdatedAt()
    {
        return $this->updated_at;
    }

    /**
     * Add tipotareas
     *
     * @param \Gitek\HotelBundle\Entity\Tipotarea $tipotareas
     * @return Tarea
     */
    public function addTipotarea(\Gitek\HotelBundle\Entity\Tipotarea $tipotareas)
    {
        $this->tipotareas[] = $tipotareas;
    
        return $this;
    }

    /**
     * Remove tipotareas
     *
     * @param \Gitek\HotelBundle\Entity\Tipotarea $tipotareas
     */
    public function removeTipotarea(\Gitek\HotelBundle\Entity\Tipotarea $tipotareas)
    {
        $this->tipotareas->removeElement($tipotareas);
    }

    /**
     * Get tipotareas
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getTipotareas()
    {
        return $this->tipotareas;
    }

    /**
     * Add categorias
     *
     * @param \Gitek\HotelBundle\Entity\Categoria $categorias
     * @return Tarea
     */
    public function addCategoria(\Gitek\HotelBundle\Entity\Categoria $categorias)
    {
        $this->categorias[] = $categorias;
    
        return $this;
    }

    /**
     * Remove categorias
     *
     * @param \Gitek\HotelBundle\Entity\Categoria $categorias
     */
    public function removeCategoria(\Gitek\HotelBundle\Entity\Categoria $categorias)
    {
        $this->categorias->removeElement($categorias);
    }

    /**
     * Get categorias
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getCategorias()
    {
        return $this->categorias;
    }

    /**
     * Add materiales
     *
     * @param \Gitek\HotelBundle\Entity\Material $materiales
     * @return Tarea
     */
    public function addMateriale(\Gitek\HotelBundle\Entity\Material $materiales)
    {
        $this->materiales[] = $materiales;
    
        return $this;
    }

    /**
     * Remove materiales
     *
     * @param \Gitek\HotelBundle\Entity\Material $materiales
     */
    public function removeMateriale(\Gitek\HotelBundle\Entity\Material $materiales)
    {
        $this->materiales->removeElement($materiales);
    }

    /**
     * Get materiales
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getMateriales()
    {
        return $this->materiales;
    }

    /**
     * Add prevenciones
     *
     * @param \Gitek\HotelBundle\Entity\Prevencion $prevenciones
     * @return Tarea
     */
    public function addPrevencione(\Gitek\HotelBundle\Entity\Prevencion $prevenciones)
    {
        $this->prevenciones[] = $prevenciones;
    
        return $this;
    }

    /**
     * Remove prevenciones
     *
     * @param \Gitek\HotelBundle\Entity\Prevencion $prevenciones
     */
    public function removePrevencione(\Gitek\HotelBundle\Entity\Prevencion $prevenciones)
    {
        $this->prevenciones->removeElement($prevenciones);
    }

    /**
     * Get prevenciones
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getPrevenciones()
    {
        return $this->prevenciones;
    }

    /**
     * Add detalles
     *
     * @param \Gitek\RegistroBundle\Entity\Registrodet $detalles
     * @return Tarea
     */
    public function addDetalle(\Gitek\RegistroBundle\Entity\Registrodet $detalles)
    {
        $this->detalles[] = $detalles;
    
        return $this;
    }

    /**
     * Remove detalles
     *
     * @param \Gitek\RegistroBundle\Entity\Registrodet $detalles
     */
    public function removeDetalle(\Gitek\RegistroBundle\Entity\Registrodet $detalles)
    {
        $this->detalles->removeElement($detalles);
    }

    /**
     * Get detalles
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getDetalles()
    {
        return $this->detalles;
    }

    /**
     * Set orden
     *
     * @param integer $orden
     * @return Tarea
     */
    public function setOrden($orden)
    {
        $this->orden = $orden;
    
        return $this;
    }

    /**
     * Get orden
     *
     * @return integer 
     */
    public function getOrden()
    {
        return $this->orden;
    }
}