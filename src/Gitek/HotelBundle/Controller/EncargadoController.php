<?php

namespace Gitek\HotelBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

use Gitek\UsuarioBundle\Entity\Usuario;
use Gitek\RegistroBundle\Entity\Master;
use Gitek\HotelBundle\Entity\Tipotarea;
use Gitek\RegistroBundle\Entity\Registro;
use Gitek\HotelBundle\Form\EncargadoasignarType;
use Gitek\RegistroBundle\Form\MasterType;

class EncargadoController extends Controller
{

    /**
     * Muestra el formulario de busqueda
     *
     */
    public function buscarAction()
    {
        $em = $this->getDoctrine()->getManager();

        $usuarios = $em->getRepository('UsuarioBundle:Usuario')->bilatuLangileak();

        return $this->render('HotelBundle:Encargado:buscar.html.twig', array(
            'usuarios' => $usuarios,
        ));
    }

    /**
     * Muestra el formulario de asignar trabajos
     *
     */
    public function asignarAction()
    {
        $em         = $this->getDoctrine()->getManager();
        $request    = $this->getRequest();
        $q          = $request->request->get('q');
        $miq0       = $request->request->get('miq0');
        $numusuarios[] = $request->request->get('usuarioshab');
        $nomusuarios[] = $request->request->get('nombreusuarioshab');
        $usuario_por_defecto = $em->getRepository('UsuarioBundle:Usuario')->findOneById($this->container->getParameter('gitek.usuario_por_defecto'));
        $servicio = $this->get('gitek_mapatareas');
        $badago = 0;

        if ( !is_null($miq0) ) {
            $q = $miq0;
        }

        if (empty($q)) {

            $q = Date('Y-m-d');
            $registros  = $em->getRepository('RegistroBundle:Registro')->registrosporfechaordenadoporhabitacion(Date($q));

            if ( count($registros) > 0 ) {
                $badago =1;
                $mapa = $servicio->getMapatareas($q);
            } else {
                $mapa = $servicio->getMapatareas($q);
                $badago = 0;
            }
        } else {

            $registros  = $em->getRepository('RegistroBundle:Registro')->registrosporfechaordenadoporhabitacion(Date($q));

            if ( count($registros) > 0 ) {
                $badago =1;

            } else {
                $badago = 0;
            }
            $mapa = $servicio->getMapatareas($q);
        }
        $usuarios   = $em->getRepository('UsuarioBundle:Usuario')->bilatuLangileak();

        if ($badago==0) {

            $master     = new Master();

            for ($i = 1; $i <= 10; $i++) {
                $reg = new Registro();
                $reg->setMaster($master);
                $mitipotarea = $em->getRepository("HotelBundle:Tipotarea")->findOneById($mapa[$i]["Tarea"]);
                $reg->setTipotarea($mitipotarea);
                if (empty($q)) {
                    $reg->setFecha(new \DateTime());
                } else {
                    $niretime = strtotime($q);
                    $n = new \DateTime(date('Y-m-d', $niretime));
                    $reg->setFecha($n);
                }
                //habitacion
                if ($i<10) {
                    $nombrehabitacion="10".$i;
                } else {
                    $nombrehabitacion="110";
                }
                $hab = $em->getRepository('HotelBundle:Habitacion')->findOneByNombre($nombrehabitacion);
                $reg->setHabitacion($hab);

                // Usuario por defecto
                $reg->setUsuario($usuario_por_defecto);

                $em->persist($reg);
            }

            $em->flush();
            $registros  = $em->getRepository('RegistroBundle:Registro')->registrosporfecha(Date($q));
            $mapa = $servicio->getMapatareas($q);
        } else {
            $i=-0;
            foreach ($registros as $r) {
                $i+=1;
                if (!is_null($numusuarios[0])) {

                    $misus = $this->getindexmapa($numusuarios[0][$i-1]);
                    $misus = $this->getindexmapa2($i);
                    $misus =$numusuarios[0][$misus];
                    $usu = $em->getRepository('UsuarioBundle:Usuario')->findOneById($misus);
                    $usuzarra=$r->getUsuario();
                    if (!is_null($usu)) {
                        $r->setUsuario($usu);
                    } else {
                      $u = $em->getRepository('UsuarioBundle:Usuario')->findOneById(999);
                      $r->setUsuario($u);
                    }
                    $em->persist($r);
                }
            }
            $em->flush();
        }

        if (!empty($registros )) {

            $r = $registros[0];

            if (!is_null($r->getMaster())) {
                $master = $em->getRepository('RegistroBundle:Master')->find($r->getMaster()->getId());
            } else {
                $master = new Master();
            }
        }

        $form    = $this->createForm(new MasterType(), $master);
        return $this->render('HotelBundle:Encargado:asignar.html.twig', array(
            'master'    => $master,
            'registros' => $registros,
            'usuarios'  => $usuarios,
            'q'         => $q,
            'numusuarios' => $numusuarios,
            'nomusuarios' => $nomusuarios,
            'mapa'      => $mapa,
            'form'      => $form->createView()
        ));
    }

    public function asignartipotareaAction()
    {
        $em      = $this->getDoctrine()->getManager();
        $master  = new Master();
        $request = $this->getRequest();
        $q       = $request->request->get('miq');
        $servicio = $this->get('gitek_mapatareas');
        $mapa = $servicio->getMapatareas($q);

        $usuarios   = $em->getRepository('UsuarioBundle:Usuario')->bilatuLangileak();

        if ('GET' === $request->getMethod()) {
            if (empty($q)) {
                $q = Date('Y-m-d');
            }

            $registros  = $em->getRepository('RegistroBundle:Registro')->registrosporfecha(Date($q));
            if (!empty($registros )) {

                $r = $registros[0];
                if ( !is_null($r->getMaster()) ) {
                    $master = $em->getRepository('RegistroBundle:Master')->find($r->getMaster()->getId());
                }
            }

        } else {

            $registros  = $em->getRepository('RegistroBundle:Registro')->registrosporfecha(Date('Y-m-d'));
            if (!empty($registros )) {
                $r = $registros[0];
                $master = $em->getRepository('RegistroBundle:Master')->find($r->getMaster()->getId());
            }
        }

        $form    = $this->createForm(new MasterType(), $master);

        return $this->render('HotelBundle:Encargado:asignartipotarea.html.twig', array(
            'master'    => $master,
            'registros' => $registros,
            'usuarios'  => $usuarios,
            'q'         => $q,
            'mapa'      => $mapa,
            'form'      => $form->createView()
        ));
    }

    public function historialAction() {
        $em         = $this->getDoctrine()->getManager();
        $request    = $this->getRequest();

        $hab        = $request->request->get('hab');

        $usu        = $request->request->get('usu');
        $q        = $request->request->get('q');

        $habitaciones   = $em->getRepository('HotelBundle:Habitacion')->findAll();
        $usuarios       = $em->getRepository('UsuarioBundle:Usuario')->bilatuLangileak();

        $registros      = $em->getRepository('RegistroBundle:Registro')->gethistoriala($hab,$usu,$q);

        return $this->render('HotelBundle:Encargado:historiala.html.twig', array(
            'registros'     => $registros,
            'habitaciones'  => $habitaciones,
            'usuarios'      => $usuarios,
            'hab'           => $hab,
            'usu'           => $usu,
            'q'             => $q,
        ));
    }

    public function dethistorialAction($id) {
        $em         = $this->getDoctrine()->getManager();

        $historial = $em->getRepository('RegistroBundle:Registro')->find($id);

        $hab = $historial->getHabitacion()->getId();
        $usu = $historial->getUsuario()->getId();
        $q = $historial->getFecha();
        $q = $q->format('Y-m-d');
        $todaslastareas =   $em->getRepository('RegistroBundle:Registro')->gethistoriala($hab,$usu,$q);
        $tipotarea =        $em->getRepository('HotelBundle:Tipotarea')->getTareasportipo($historial->getTipotarea()->getId());

        if (!$historial) {
            throw $this->createNotFoundException('Unable to find Habitacion entity.');
        }

        return $this->render('HotelBundle:Encargado:dethistoriala.html.twig', array(
            'historial'     => $historial,
            'tipotarea'     => $tipotarea,
            'todaslastareas'=> $todaslastareas,
        ));
    }

    protected function getindexmapa($i) {
        // 0 => 107
        // 1 => 108
        // 2 => 109
        // 3 => 110
        // 4 => 106
        // 5 => 105
        // 6 => 104
        // 7 => 103
        // 8 => 102
        // 9 => 101


        $i = (int)$i;
        switch ($i-1){
            case 0:
                return 9;
            case 1:
                return 8;
            case 2:
                return 7;
            case 3:
                return 6;
            case 4:
                return 5;
            case 5:
                return 4;
            case 6:
                return 0;
            case 7:
                return 1;
            case 8:
                return 2;
            case 9:
                return 3;
        }
    }

    protected function getindexmapa2($i) {
        // 0 => 101
        // 1 => 102
        // 2 => 103
        // 3 => 114
        // 4 => 105
        // 5 => 106
        // 6 => 107
        // 7 => 108
        // 8 => 109
        // 9 => 110


        $i = (int)$i;
        switch ($i-1){
            case 0:
                return 9;
            case 1:
                return 8;
            case 2:
                return 7;
            case 3:
                return 6;
            case 4:
                return 5;
            case 5:
                return 4;
            case 6:
                return 0;
            case 7:
                return 1;
            case 8:
                return 2;
            case 9:
                return 3;
            case "":
                return 999;
        }
    }

    protected function getUsuario($id)
    {
        $em = $this->getDoctrine()->getManager();
        $usuario = $em->getRepository('UsuarioBundle:Usuario')->find($id);
        if (!$usuario) {
            throw $this->createNotFoundException('Ez da aurkitu.');
        }
        return $usuario;
    }

    protected function getTipotarea($id)
    {
        $em = $this->getDoctrine()->getManager();
        $tipotarea = $em->getRepository('HotelBundle:Tipotarea')->find($id);
        if (!$tipotarea) {
            throw $this->createNotFoundException('Ez da aurkitu.');
        }
        return $tipotarea;
    }

    protected function getHabitacion($id)
    {
        $em = $this->getDoctrine()->getManager();
        $habitacion = $em->getRepository('HotelBundle:Habitacion')->find($id);
        if (!$habitacion) {
            throw $this->createNotFoundException('Ez da aurkitu.');
        }
        return $habitacion;
    }

}
