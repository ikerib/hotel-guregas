
$('.sel_user').click(function(){

    var mirel = $(this).attr('data-rel');
    
    $('#username').val(mirel);

    if((mirel==='admin') || (mirel==='recepcion') || (mirel=='Recepcion') || (mirel==='encargado') || (mirel=='Encargado')) {
        $(this).simpledialog({
            'mode' : 'string',
            'prompt' : 'zein da pasahitza?',
            'inputPassword': true,
            'buttons' : {
                'OK': {
                    click: function () {
                        $('#password').val($(this).attr('data-string'));
                        if(mirel==='admin' ) {
                            $('#_target_path').val('/backend');
                        } else if ((mirel==='recepcion') || (mirel=='Recepcion')){
                            $('#_target_path').val('/recepcion');
                        } else if ((mirel==='encargado') || (mirel=='Encargado')){
                            $('#_target_path').val('/encargado');
                        }

                        $('#_submit').click()
                    }
                },
                'Cancel': {
                    click: function () { },
                        icon: "delete",
                        theme: "c"
                }
            }
        });
    } else {
        $('#_submit').click()
    }
});